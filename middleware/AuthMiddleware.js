const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

const UserModel = require('../models/Users');

module.exports = async(req, res, next) => {
    // TODO: Decide between header or cookie
    console.log(req.headers.authorization)
    let accessToken = req.cookies.accessToken || req.headers.authorization;
    if (!accessToken) {
        throw new Error("Invalid Token");
    }
    let user = jwt.verify(accessToken, process.env.JWT_SECRET);

    req.user = user;

    next();
};