"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("consumer_images", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            image_name: {
                type: Sequelize.STRING,
            },
            folder_path: {
                type: Sequelize.STRING,
            },
            link: {
                type: Sequelize.STRING,
            },
            module_id: {
                type: Sequelize.INTEGER, // 1 for news, 2 for campaign, 3 for campaign entry
            },
            created_by: {
                type: Sequelize.STRING,
            },
            updated_by: {
                type: Sequelize.STRING,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("consumer_images");
    },
};