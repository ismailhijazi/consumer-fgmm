"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("consumer_promo_files", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            file_name: {
                type: Sequelize.STRING,
            },
            folder_path: {
                type: Sequelize.STRING,
            },
            link: {
                type: Sequelize.STRING,
            },
            fk_campaign_id: {
                type: Sequelize.INTEGER,
            },
            created_by: {
                type: Sequelize.STRING,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("consumer_promo_files");
    },
};