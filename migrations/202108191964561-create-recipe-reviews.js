"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("consumer_recipe_reviews", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            fk_recipe_id: {
                type: Sequelize.INTEGER,
                references: { model: 'consumer_recipes', key: 'id' }
            },
            fk_user_id: {
                type: Sequelize.INTEGER,
                references: { model: 'consumer_users', key: 'id' }
            },
            rating: {
                type: Sequelize.FLOAT,
            },
            review_date: {
                type: Sequelize.DATEONLY,
                defaultValue: Sequelize.NOW
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            status: {
                type: Sequelize.INTEGER, // 0 is for waiting, 1 is for approved, 2 is for reject
                defaultValue: 0
            },
            created_by: {
                type: Sequelize.INTEGER,
            },
            updated_by: {
                type: Sequelize.INTEGER,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        }).then(() => queryInterface.addIndex('consumer_recipe_reviews', ['fk_recipe_id', 'fk_user_id']));
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("consumer_recipe_reviews");
    },
};