"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("types", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            category_id: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            fk_image_id: {
                type: Sequelize.INTEGER,
            },
            active: {
                type: Sequelize.INTEGER,
                defaultValue: '1'
            },
            created_by: {
                type: Sequelize.STRING,
                allowNull: false,
                defaultValue: '0'
            },
            updated_by: {
                type: Sequelize.STRING,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("types");
    },
};