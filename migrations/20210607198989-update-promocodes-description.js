module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('consumer_promo_codes', 'description', {
                type: Sequelize.TEXT,
                allowNull: true,
            })
        ])
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('consumer_promo_codes', 'description', {
                type: Sequelize.STRING,
                allowNull: true,
            })
        ])
    }
};