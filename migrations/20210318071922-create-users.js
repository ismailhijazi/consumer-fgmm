"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("consumer_users", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            first_name: {
                type: Sequelize.STRING,
            },
            middle_name: {
                type: Sequelize.STRING,
            },
            last_name: {
                type: Sequelize.STRING,
            },
            gender: {
                type: Sequelize.STRING,
            },
            phone: {
                type: Sequelize.STRING,
            },
            email: {
                type: Sequelize.STRING,
            },
            date_of_birth: {
                type: Sequelize.DATE,
            },
            state: {
                type: Sequelize.STRING,
            },
            password: {
                type: Sequelize.STRING,
            },
            language: {
                type: Sequelize.STRING,
            },
            username: {
                type: Sequelize.STRING,
            },
            fk_access_token_id: {
                type: Sequelize.INTEGER,
            },
            created_by: {
                type: Sequelize.INTEGER,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("consumer_users");
    },
};