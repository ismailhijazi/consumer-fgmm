"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("consumer_news", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            title: {
                type: Sequelize.STRING,
            },
            description: {
                type: Sequelize.STRING,
            },
            fk_image_id: {
                type: Sequelize.INTEGER,
            },
            publish_status: {
                type: Sequelize.INTEGER,
            },
            is_feature: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            start_date: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            end_date: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },

            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("consumer_news");
    },
};