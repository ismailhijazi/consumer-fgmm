"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("consumer_recipes", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            title: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            fk_type_id: {
                type: Sequelize.INTEGER,
                references: { model: 'types', key: 'id' }
            },
            persons: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            estimated_time: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            ingredients: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            active: {
                type: Sequelize.INTEGER,
            },
            created_by: {
                type: Sequelize.STRING,
            },
            updated_by: {
                type: Sequelize.STRING,
            },
            deleted_at: {
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: Sequelize.NOW
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        }).then(() => queryInterface.addIndex('consumer_recipes', ['fk_type_id']));
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("consumer_recipes");
    },
};