module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('consumer_images', 'type', {
                type: Sequelize.STRING,
                allowNull: true,
            })
        ])
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('consumer_images', 'type', {
                type: Sequelize.STRING,
                allowNull: true,
            })
        ])
    }
};