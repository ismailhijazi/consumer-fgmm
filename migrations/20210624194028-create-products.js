"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("products", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            weight: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            pieces: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            types_id: {
                type: Sequelize.INTEGER,
                references: { model: 'types', key: 'id' }
            },
            product_type_id: Sequelize.STRING,
            item_code: Sequelize.BIGINT,
            item_name: Sequelize.STRING,
            item_group_code: Sequelize.BIGINT,
            item_group_name: Sequelize.STRING,
            canceled: Sequelize.STRING,
            default_image: Sequelize.INTEGER,
            active: Sequelize.INTEGER,
            is_feature: Sequelize.INTEGER,
            created_by:Sequelize.STRING,
            updated_by: Sequelize.STRING,
            deleted_at: {
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: Sequelize.NOW
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        }).then(() => queryInterface.addIndex('products', ['types_id']));
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("products");
    },
};