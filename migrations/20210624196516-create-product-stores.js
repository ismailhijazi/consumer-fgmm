"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("consumer_product_stores", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            fk_product_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                references: { model: 'products', key: 'id' }
            },
            fk_store_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                references: { model: 'consumer_stores', key: 'id' }
            },
            created_by: {
                type: Sequelize.INTEGER,
            },
            updated_by: {
                type: Sequelize.INTEGER,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        }).then(() => queryInterface.addIndex('consumer_product_stores', ['fk_product_id', 'fk_store_id']));
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("consumer_product_stores");
    },
};