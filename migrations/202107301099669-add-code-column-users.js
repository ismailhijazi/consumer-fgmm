module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('consumer_users', 'verification_code', {
                type: Sequelize.STRING,
                allowNull: true,
            }),
            queryInterface.addColumn('consumer_users', 'code_used', {
                type: Sequelize.BOOLEAN,
                allowNull: true,
                defaultValue: false
            })
        ])
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.dropColumn('consumer_users', 'verification_code'),
            queryInterface.dropColumn('consumer_users', 'code_used')
        ])
    }
};