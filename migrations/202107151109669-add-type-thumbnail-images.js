module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('consumer_images', 'thumbnail', {
                type: Sequelize.STRING,
                allowNull: true,
                defaultValue: ""
            })
        ])
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.dropColumn('consumer_images', 'thumbnail', {
                type: Sequelize.STRING,
                allowNull: true,
            })
        ])
    }
};