"use strict";
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable("consumer_user_recipes", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            fk_recipe_id: {
                type: Sequelize.INTEGER,
                references: { model: 'consumer_recipes', key: 'id' }
            },
            fk_user_id: {
                type: Sequelize.INTEGER,
                references: { model: 'consumer_users', key: 'id' }
            },
            created_by: {
                type: Sequelize.INTEGER,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW
            },
        }).then(() => queryInterface.addIndex('consumer_user_recipes', ['fk_recipe_id', 'fk_user_id']));
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable("consumer_user_recipes");
    },
};