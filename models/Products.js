'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Products extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            this.belongsTo(models.Types, { foreignKey: 'types_id' });
            this.hasMany(models.ProductImages, { foreignKey: 'fk_product_id' });
            this.hasMany(models.ProductStores, { foreignKey: 'fk_product_id' });
            this.hasMany(models.RecipeProducts, { foreignKey: 'fk_product_id' });
        }
    };
    Products.init({
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        weight: DataTypes.STRING,
        pieces: DataTypes.STRING,
        types_id: DataTypes.INTEGER,
        product_type_id: DataTypes.STRING,
        item_code: DataTypes.BIGINT,
        item_name: DataTypes.STRING,
        item_group_code: DataTypes.BIGINT,
        item_group_name: DataTypes.STRING,
        canceled: DataTypes.STRING,
        active: DataTypes.INTEGER,
        is_feature: DataTypes.INTEGER,
        default_image: DataTypes.INTEGER,
        created_by: DataTypes.STRING,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'Products',
        tableName: "products",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return Products;
};