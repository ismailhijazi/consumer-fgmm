'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Stores extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Images, { foreignKey: 'fk_image_id' });
            this.hasMany(models.ProductStores, { foreignKey: 'fk_store_id' });
            this.hasMany(models.RecipeStores, { foreignKey: 'fk_store_id' });
        }
    };
    Stores.init({
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        fk_image_id: DataTypes.INTEGER,
        location: DataTypes.STRING,
        created_by: DataTypes.STRING,
        updated_by: DataTypes.STRING,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'Stores',
        tableName: "consumer_stores",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return Stores;
};