'use strict';
const {
    Model,
    Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class RecipeStores extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Recipes, { foreignKey: 'fk_recipe_id' });
            this.belongsTo(models.Stores, { foreignKey: 'fk_store_id' });
        }
    };
    RecipeStores.init({
        fk_recipe_id: DataTypes.INTEGER,
        fk_store_id: DataTypes.INTEGER,
        created_by: DataTypes.INTEGER,
        updated_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'RecipeStores',
        tableName: "consumer_recipe_stores",
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return RecipeStores;
};