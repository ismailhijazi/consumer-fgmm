'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class States extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    States.init({
        name: DataTypes.STRING,
        iso2_code: DataTypes.STRING,
        iso3_code: DataTypes.STRING,
        lat: DataTypes.STRING,
        lng: DataTypes.STRING,
        country_id: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'States',
        tableName: "states",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return States;
};