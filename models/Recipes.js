'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Recipes extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            this.belongsTo(models.Types, { foreignKey: 'fk_type_id' });
            this.hasMany(models.RecipeMedia, { foreignKey: 'fk_recipe_id' });
            this.hasMany(models.RecipeStores, { foreignKey: 'fk_recipe_id' });
            this.hasMany(models.RecipeProducts, { foreignKey: 'fk_recipe_id' });
            this.hasMany(models.RecipeReviews, { foreignKey: 'fk_recipe_id' });
            this.hasMany(models.UsersRecipe, { foreignKey: 'fk_recipe_id' });
        }
    };
    Recipes.init({
        fk_type_id: DataTypes.INTEGER,
        title: DataTypes.STRING,
        description: DataTypes.TEXT,
        estimated_time: DataTypes.STRING,
        persons: DataTypes.STRING,
        active: DataTypes.INTEGER,
        ingredients: DataTypes.TEXT,
        created_by: DataTypes.STRING,
        updated_by: DataTypes.STRING,
        deleted_at: DataTypes.DATE,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'Recipes',
        tableName: "consumer_recipes",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return Recipes;
};