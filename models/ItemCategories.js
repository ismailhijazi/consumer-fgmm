'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class ItemCategories extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    ItemCategories.init({
        uuid: DataTypes.INTEGER,
        type_id: DataTypes.INTEGER,
        name: DataTypes.STRING,
        item_group_code: DataTypes.INTEGER,
        description: DataTypes.STRING,
        active: DataTypes.INTEGER,
        created_by: DataTypes.INTEGER,
        deleted_at: DataTypes.DATE,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'ItemCategories',
        tableName: "item_categories",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return ItemCategories;
};