'use strict';
const {
    Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Users extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    Users.init({
        first_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        middle_name: DataTypes.STRING,
        last_name: {
            type: DataTypes.STRING,
        },
        gender: {
            type: DataTypes.STRING,
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        date_of_birth: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        state: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        language: DataTypes.STRING,
        username: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        verification_code: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        code_used: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },
        fk_access_token_id: DataTypes.INTEGER,
        created_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'Users',
        tableName: "consumer_users",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return Users;
};