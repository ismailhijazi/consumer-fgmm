'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Images extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.hasMany(models.CampaignImages, { foreignKey: 'fk_image_id' });
            this.hasMany(models.ProductImages, { foreignKey: 'fk_image_id' });
        }
    };
    Images.init({
        image_name: DataTypes.STRING,
        folder_path: DataTypes.STRING,
        link: DataTypes.STRING,
        module_id: DataTypes.INTEGER, // 1 for news, 2 for campaign, 3 for campaign entry, 4 for category, 5 for stores, 6 for products, 7 for recipes
        type: DataTypes.STRING,
        thumbnail: DataTypes.STRING,
        created_by: DataTypes.STRING,
        updated_by: DataTypes.STRING,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'Images',
        tableName: "consumer_images",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return Images;
};