'use strict';
const {
    Model,
    Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class RecipeMedia extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Recipes, { foreignKey: 'fk_recipe_id' });
            this.belongsTo(models.Images, { foreignKey: 'fk_image_id' });
        }
    };
    RecipeMedia.init({
        fk_recipe_id: DataTypes.INTEGER,
        fk_image_id: DataTypes.INTEGER,
        created_by: DataTypes.INTEGER,
        updated_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'RecipeMedia',
        tableName: "consumer_recipe_media",
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return RecipeMedia;
};