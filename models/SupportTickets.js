'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class SupportTickets extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Users, { foreignKey: 'fk_user_id' });
        }
    };
    SupportTickets.init({
        message: DataTypes.STRING,
        fk_user_id: DataTypes.INTEGER,
        status: DataTypes.INTEGER, // 0 - New, 1 - Assigned, 2 - Resolved, 3 - Closed
        created_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'SupportTickets',
        tableName: "consumer_support_tickets",
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return SupportTickets;
};