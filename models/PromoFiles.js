'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class PromoFiles extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Campaigns, { foreignKey: 'fk_campaign_id' });
        }
    };
    PromoFiles.init({
        file_name: DataTypes.STRING,
        folder_path: DataTypes.STRING,
        link: DataTypes.STRING,
        fk_campaign_id: DataTypes.INTEGER,
        created_by: DataTypes.STRING,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'PromoFiles',
        tableName: "consumer_promo_files",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return PromoFiles;
};