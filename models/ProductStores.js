'use strict';
const {
    Model,
    Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class ProductStores extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Products, { foreignKey: 'fk_product_id' });
            this.belongsTo(models.Stores, { foreignKey: 'fk_store_id' });
        }
    };
    ProductStores.init({
        fk_product_id: DataTypes.INTEGER,
        fk_store_id: DataTypes.INTEGER,
        created_by: DataTypes.INTEGER,
        updated_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'ProductStores',
        tableName: "consumer_product_stores",
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return ProductStores;
};