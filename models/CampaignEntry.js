'use strict';
const {
    Model,
    Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class CampaignEntry extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Users, { foreignKey: 'fk_user_id' });
            this.belongsTo(models.Images, { foreignKey: 'fk_image_id' });
        }
    };
    CampaignEntry.init({
        fk_user_id: DataTypes.INTEGER,
        fk_campaign_id: DataTypes.INTEGER,
        fk_promo_id: DataTypes.INTEGER,
        fk_image_id: DataTypes.INTEGER,
        is_a_winner: DataTypes.BOOLEAN,
        created_by: DataTypes.INTEGER,
        updated_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'CampaignEntry',
        tableName: "consumer_campaign_entries",
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return CampaignEntry;
};