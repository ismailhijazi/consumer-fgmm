'use strict';
const {
    Model,
    Op
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Campaigns extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.hasMany(models.PromoCodes, { foreignKey: 'fk_campaign_id' });
            this.hasMany(models.CampaignEntry, { foreignKey: 'fk_campaign_id' });
            this.hasMany(models.CampaignImages, { foreignKey: 'fk_campaign_id' });
            this.hasMany(models.PromoFiles, { foreignKey: 'fk_campaign_id' });

        }
    };
    Campaigns.init({
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        campaign_type: DataTypes.INTEGER, // 1 - promo, 2 - competition
        start_date: DataTypes.DATE,
        end_date: DataTypes.DATE,
        created_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'Campaigns',
        tableName: "consumer_campaigns",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return Campaigns;
};