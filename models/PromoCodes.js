'use strict';
const {
    Model,
    Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class PromoCodes extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Campaigns, { foreignKey: 'fk_campaign_id' });
            this.hasMany(models.CampaignEntry, { foreignKey: 'fk_promo_id' });
        }
    };
    PromoCodes.init({
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        fk_campaign_id: DataTypes.INTEGER,
        code: DataTypes.STRING,
        start_date: DataTypes.DATE,
        end_date: DataTypes.DATE,
        created_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'PromoCodes',
        tableName: "consumer_promo_codes",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return PromoCodes;
};