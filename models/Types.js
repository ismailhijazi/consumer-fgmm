'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Types extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Images, { foreignKey: 'fk_image_id' });
            this.hasMany(models.Products, { foreignKey: 'types_id' });
            this.hasMany(models.Recipes, { foreignKey: 'fk_type_id' });
        }
    };
    Types.init({
        category_id: DataTypes.STRING,
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        fk_image_id: DataTypes.INTEGER,
        active: DataTypes.INTEGER,
        created_by: DataTypes.STRING,
        updated_by: DataTypes.STRING,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'Types',
        tableName: "types",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return Types;
};