'use strict';
const {
    Model,
    Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class ProductImages extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Products, { foreignKey: 'fk_product_id' });
            this.belongsTo(models.Images, { foreignKey: 'fk_image_id' });
        }
    };
    ProductImages.init({
        fk_product_id: DataTypes.INTEGER,
        fk_image_id: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'ProductImages',
        tableName: "consumer_product_images",
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return ProductImages;
};