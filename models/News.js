'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class News extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Images, { foreignKey: 'fk_image_id' });
        }
    };
    News.init({
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        fk_image_id: DataTypes.INTEGER,
        publish_status: DataTypes.INTEGER, // 1 - draft, 2 - published, 3 - expired
        is_feature: DataTypes.BOOLEAN,
        start_date: DataTypes.DATE,
        end_date: DataTypes.DATE,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'News',
        tableName: "consumer_news",
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return News;
};