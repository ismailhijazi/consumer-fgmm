'use strict';
const {
    Model,
    Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class RecipeProducts extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Recipes, { foreignKey: 'fk_recipe_id' });
            this.belongsTo(models.Products, { foreignKey: 'fk_product_id' });
        }
    };
    RecipeProducts.init({
        fk_recipe_id: DataTypes.INTEGER,
        fk_product_id: DataTypes.INTEGER,
        created_by: DataTypes.INTEGER,
        updated_by: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'RecipeProducts',
        tableName: "consumer_recipe_products",
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    });

    return RecipeProducts;
};