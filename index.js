require('dotenv').config();
require('express-async-errors');
var path = require('path');
const cors = require('cors');

const express = require('express');
const expHbs = require('express-handlebars');
var cookieParser = require('cookie-parser');

const errorMiddleware = require('./middleware/ErrorMiddleware');
const authMiddleware = require('./middleware/AuthMiddleware');

const app = express();
const port = process.env.PORT;
app.get('/', function(req, res) {
    res.header('Content-Type', 'text/html').send("Server Start on port: " + port);
});
// Add a list of allowed origins.
// If you have more origins you would like to add, you can add them to the array below.
const allowedOrigins = ['http://localhost:3000', 'http://localhost:3001', 'http://consumer.fgmmdev.com'];

const options = {
  origin: allowedOrigins
};
app.use(cors(options)); /* NEW */
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});
app.use(express.static(path.join(__dirname, 'public')));
// Authentication middleware
app.use(cookieParser());

/**
 * server config.
 */
app.listen(port, () => {
    console.log(`Example app listening at port http://localhost:${port}`)
})


/**
 * Routes..
 */
require('./routes/User')(app, authMiddleware);
require('./routes/News')(app, authMiddleware);
require('./routes/Campaign')(app, authMiddleware);
require('./routes/PromoCode')(app, authMiddleware);
require('./routes/Image')(app, authMiddleware);
require('./routes/SupportTicket')(app, authMiddleware);
require('./routes/Product')(app, authMiddleware);
require('./routes/ItemCategory')(app, authMiddleware);
require('./routes/State')(app, authMiddleware);
require('./routes/CampaignImages')(app, authMiddleware);
require('./routes/Stores')(app, authMiddleware);
require('./routes/Types')(app, authMiddleware);
require('./routes/ProductImages')(app, authMiddleware);
require('./routes/ProductStore')(app, authMiddleware);
require('./routes/Recipe')(app, authMiddleware);
require('./routes/MobileApp')(app, authMiddleware);
require('./routes/UsersRecipe')(app, authMiddleware);
require('./routes/RecipeReviews')(app, authMiddleware);


app.use(errorMiddleware);