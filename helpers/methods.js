/**
 *
 * @param message
 * @param payload
 * @returns {{payload, message, status: boolean}}
 */
exports.successResponse = (message, payload) => {
    return {
        status: true,
        message: message,
        payload: payload
    }
}

/**
 *
 * @param message
 * @returns {{message, status: boolean}}
 */
exports.errorResponse = message => {
    return {
        status: false,
        message: message,
    }
}

/**
 *
 * @type {{message: string, status: boolean}}
 */
exports.notFountResponse = {
    status: false,
    message: 'Unable to find the requested resource!',
}

/**
 *
 * @param model
 * @param id
 * @param item
 * @returns {{id, created: boolean}}
 */
exports.updateOrCreate = async(model, id, item) => {
    if (id) {
        // if id then update
        const updated = await model.update(item, { where: { id } });
        if (updated) {
            const updatedItem = await model.findByPk(id)
            return {
                item: updatedItem,
                created: false
            }
        }
    } else {
        // if not id then create a new one
        const newItem = await model.create(item)
        return { item: newItem, created: true };
    }
}