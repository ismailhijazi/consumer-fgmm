const {
    Op
} = require('sequelize');
const { Campaigns, Images, PromoCodes, CampaignEntry, CampaignImages, PromoFiles } = require('../models/Index');
const methods = require('../helpers/methods');

class CampaignLogic {

}

CampaignLogic.createCampaign = async(data) => {
    const item = {
        title: data.title,
        description: data.description,
        campaign_type: data.campaign_type, // 1 - promo, 2 - competition
        start_date: new Date(data.start_date),
        end_date: new Date(data.end_date),
        created_by: data.userID
    };
    return await methods.updateOrCreate(Campaigns, data.id, item)
};

CampaignLogic.getActiveCampaigns = async() => {
    return await Campaigns.findAll({
        attributes: ['id', 'title', 'description', 'campaign_type','start_date', 'end_date'],
        where: {
            start_date: {
                [Op.lte]: new Date()
            },
            end_date: {
                [Op.gte]: new Date()
            }
        },
        order: [
            ['updated_at', 'DESC']
        ],
        include: 
            [{
                model: PromoCodes,
                attributes: ['id', 'title', 'description', 'code','start_date', 'end_date']
            }, {
                model: CampaignImages,
                attributes: ['id'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }]
    })
};

CampaignLogic.getCampaignByID = async(id) => {
    return await Campaigns.findByPk(id, { 
        attributes: ['id', 'title', 'description', 'campaign_type','start_date', 'end_date'],
        include: 
        [{
            model: PromoCodes,
            attributes: ['id', 'title', 'description', 'code','start_date', 'end_date']
        }, {
            model: CampaignImages,
            attributes: ['id'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        },{
            model: PromoFiles,
            attributes: ['id', 'file_name']
        }]
    });
};

CampaignLogic.deleteCampaign = async(id) => {
    const allCampaign = await Campaigns.findByPk(id, { include: [CampaignEntry, PromoCodes] })
    if (allCampaign.CampaignEntries.length > 0 || allCampaign.PromoCodes.length > 0)
        return "Campaign can't be deleted"
    else
        return await Campaigns.destroy({
            where: {
                id
            }
        })
};

CampaignLogic.deleteMultipleCampaign = async(idArray) => {
    const promises =  idArray.map(async(id) => {
        const allCampaign = await Campaigns.findByPk(id, {
            include: [CampaignEntry, PromoCodes],
        });
        if(allCampaign.CampaignEntries.length === 0 && allCampaign.PromoCodes.length === 0){
            await Campaigns.destroy({
                    where: {
                        id
                    }
                })
            return "Campaign with ID: "+id+" is deleted."
        } else {
            return "Campaign with ID: "+id+" is associated with Promos, Can't delete."
        }
    })
    return Promise.all(promises)
};
module.exports = CampaignLogic