const { Users } = require('../models/Index');
const {
    Op
} = require('sequelize');
const methods = require('../helpers/methods');
const bcrypt = require('bcrypt');
const hashSalt = parseInt(process.env.HASH_SALT) || 10;

const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];
var nodemailer = require('nodemailer');

class UsersLogic {

}

UsersLogic.createUser = async(data) => {
    if (!data.email)
        throw Error("Email is required");

    let existingUsers = [];
    if(!data.id)
        existingUsers = await Users.findAll({
            where: {
                username: data.email
            }
        });
    else {
        existingUsers = await Users.findAll({
            where: {
                id: data.id
            }
        });
    }

    if (existingUsers.length && !data.id) {
        throw Error("Email is already taken");
    }

    const hashedPassword = await bcrypt.hash(data.password, hashSalt);

    const item = {
        first_name: data.first_name,
        middle_name: data.middle_name,
        last_name: data.last_name,
        gender: data.gender,
        phone: data.phone,
        email: data.email,
        date_of_birth: data.date_of_birth,
        state: data.state,
        language: data.language,
        username: (data.id)?data.username:data.email
    };
    if(!data.id || data.password)
        item.password = hashedPassword
    return await methods.updateOrCreate(Users, data.id, item)

};

UsersLogic.changePassword = async(data) => {
    let userDetails = await Users.findAll({
        where: {
            username: data.username
        }
    });

    if (!userDetails.length) {
        throw Error("User does not exist");
    }

    let isOldPasswordValid = await bcrypt.compareSync(data.oldPassword, userDetails[0].dataValues.password);

    if (isOldPasswordValid) {
        const hashedPassword = await bcrypt.hash(data.newPassword, hashSalt);
        return await Users.update({
            password: data.newPassword,
            updated_at: new Date()
        }, {
            where: {
                username: data.username
            }
        });
    } else {
        throw Error("Old Password is wrong");
    }
};

UsersLogic.validateUser = async(data) => {
    let user = await Users.findAll({
        where: {
            username: data.username
        },
        attributes: ['id', 'username','first_name','last_name','phone', 'email', 'state', 'password']
    });
    if (user.length) {
        user = user[0];
        let isUserValid = await bcrypt.compareSync(data.password, user.password);
        delete user.password;
        if(isUserValid)
            return {
                "id": user.id,
                "username": user.username,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "phone": user.phone,
                "email": user.email,
                "state": user.state
            }
        else {
            throw Error("Invalid password");
        }
        
    } else {
        throw Error("Invalid username");
    }
};

UsersLogic.validateAdminUser = async(data) => {
    if(config.adminData.username === data.username && 
        config.adminData.password === data.password) {
            return config.adminData
    }
};

UsersLogic.findUserById = async(id) => {
    return await Users.findByPk(id)
};

UsersLogic.getAllUsers = async() => {
    return await Users.findAll({
        order: [
            ['updated_at', 'DESC']
        ]
    })
};

UsersLogic.sendEmail = async(data) => {
    const result = Math.random().toString(36).substring(2,8);
    var existingUsers = await Users.findAll({
        where: {
            username: data.phone
        }
    });
    if (existingUsers.length){
       
        var mail = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'ghazaltaimur27@gmail.com',
                pass: 'tcuodwhpqeovkxep'
            }
        });

        var mailOptions = {
            from: 'ghazaltaimur27@gmail.com',
            to: data.email,
            subject: 'Forget Password Verification Code',
            html: 'Dear,'+
            'Please enter this code for changing the password. '+
            'Code is: <b>'+result+'</b>'
        }
        
        mail.sendMail(mailOptions, async function(error, info){
            if (error) {
                throw Error("Can't send an email");
            } else {
                const item = { 
                    verification_code : result,
                    code_used: false
                }
                await methods.updateOrCreate(Users, existingUsers[0].id, item)
                return "Email sent"
            }
        });
    } else {
        throw Error("Phone number is invalid");
    }
};

UsersLogic.verifyEmailCode = async(data) => {
    var existingUsers = await Users.findAll({
        where: {
            phone: data.phone,
            email: data.email,
        }
    });
    if(existingUsers.length > 0) {
        var user = existingUsers[0].dataValues

        if(user.verification_code === data.code && user.code_used === false) {
            const item = { 
                code_used : true
            }
            await methods.updateOrCreate(Users, user.id, item)
            return "Code is verified"
        } else if(user.verification_code !== data.code && user.code_used === false) {
            throw Error("Code does not match");
        } else {
            throw Error("Code already used");
        }
    } else {
        throw Error("Phone and email does not match");
    }
};

UsersLogic.PasswordReset = async(data) => {
    let userDetails = await Users.findAll({
        where: {
            username: data.username
        }
    });

    if (!userDetails.length) {
        throw Error("User does not exist");
    }

    const hashedPassword = await bcrypt.hash(data.newPassword, hashSalt);
        return await Users.update({
            password: data.newPassword,
            updated_at: new Date()
        }, {
            where: {
                username: data.username
            }
        });
   
};

UsersLogic.editUser = async(data) => {
    if(!data.id)
        throw Error("You can not change the user detail");

    let hashedPassword = '';
    if(data.password)
        hashedPassword = await bcrypt.hash(data.password, hashSalt);

    const item = {
        first_name: data.first_name,
        middle_name: data.middle_name,
        last_name: data.last_name,
        gender: data.gender,
        phone: data.phone,
        date_of_birth: data.date_of_birth,
        state: data.state,
        language: data.language
    };
    if(data.id && data.password)
        item.password = hashedPassword
    return await methods.updateOrCreate(Users, data.id, item)

};
module.exports = UsersLogic