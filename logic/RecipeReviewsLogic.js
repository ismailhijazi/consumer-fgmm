const {
    Op
} = require('sequelize');
const { RecipeReviews, Recipes, RecipeMedia, Users, Images } = require('../models/Index');
const methods = require('../helpers/methods');

class RecipeReviewsLogic {

}

RecipeReviewsLogic.createRecipeReviews = async(data, userID) => {
    const item = {
            fk_user_id: data.user_id?data.user_id:userID,
            fk_recipe_id: data.recipe_id,
            rating:  data.rating,
            description:  data.description,
            review_date:  data.review_date,
            created_by: userID,
            updated_by: userID
    };

    return await methods.updateOrCreate(RecipeReviews, data.id, item)
    
};


RecipeReviewsLogic.getAllRecipeReviews = async(data) => {
    return await RecipeReviews.findAll({
        attributes: ['id', 'fk_recipe_id', 'fk_user_id', 'rating','description',
        'review_date', 'status'],
        include: [
        {
            model: Recipes,
            attributes:  ['id', 'title', 'description', 'estimated_time','persons',
                'ingredients'],
            include: [{
                model: RecipeMedia,
                attributes: ['id'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }]
        }, 
        {
            model: Users,
            attributes: ['id', 'first_name', 'middle_name', 'last_name']
        }],
        order: [
            ['updated_at', 'DESC']
        ]
    });
};



RecipeReviewsLogic.getAllRecipeReviewsByRecipeId = async(id) => {
    return await RecipeReviews.findAll({
        attributes: ['id', 'fk_recipe_id', 'fk_user_id', 'rating','description',
        'review_date'],
        where: {
            fk_recipe_id: id
        },
        include: [{
            model: Users,
            attributes: ['id', 'first_name', 'middle_name', 'last_name']
        }],
        order: [
            ['updated_at', 'DESC']
        ]
    });
};


RecipeReviewsLogic.updateRecipeReviewStatus = async(data, userID) => {
    const item = {
            status:data.status,
            updated_by: userID
    };

    return await methods.updateOrCreate(RecipeReviews, data.id, item)
    
};

module.exports = RecipeReviewsLogic