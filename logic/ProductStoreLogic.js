const {
    Op
} = require('sequelize');
const { ProductStores } = require('../models/Index');
const methods = require('../helpers/methods');

class ProductStoreLogic {

}

ProductStoreLogic.createProductStores = async(data, userID) => {
    ProductStores.destroy({
        where: {
           fk_product_id: data[0].fk_product_id
        }
    })
    data.forEach(async element => {
        const item = {
            fk_store_id: element.fk_store_id,
            fk_product_id	: element.fk_product_id,
            created_by: userID,
            updated_by: userID
        };

        await methods.updateOrCreate(ProductStores, data.id, item)
    });
    
};

module.exports = ProductStoreLogic