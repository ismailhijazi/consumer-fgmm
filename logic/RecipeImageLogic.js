const {
    Op
} = require('sequelize');
const { RecipeMedia } = require('../models/Index');
const methods = require('../helpers/methods');

class RecipeImageLogic {

}

RecipeImageLogic.createRecipeImage = async(data, userID) => {
    data.forEach(async element => {
        const item = {
            fk_image_id: element.fk_image_id,
            fk_recipe_id	: element.fk_recipe_id,
            created_by: userID,
            updated_by: userID
        };

        await methods.updateOrCreate(RecipeMedia, data.id, item)
    });
    
};

module.exports = RecipeImageLogic