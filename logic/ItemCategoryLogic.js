const { ItemCategories } = require('../models/Index');

class ItemCategoryLogic {

}

ItemCategoryLogic.getProductCategories = async() => {
    return await ItemCategories.findAll();
};

module.exports = ItemCategoryLogic