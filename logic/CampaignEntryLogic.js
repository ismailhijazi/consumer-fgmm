const { CampaignEntry } = require('../models/Index');
const methods = require('../helpers/methods')

class CampaignEntryLogic {

}

CampaignEntryLogic.createCampaignEntry = async(data, user) => {
    const entry = await CampaignEntry.create({
        "fk_user_id": user.id,
        "fk_campaign_id": data.campaignID || 0,
        "fk_promo_id": data.promoID || 0,
        "fk_image_id": data.imageID || 0,
        "is_a_winner": 0,
        "created_by": user.id,
        "updated_by": user.id
    });
    return entry;
};

CampaignEntryLogic.enterIntoCampaignCompetition = async(req, user, response) => {
    const data = req.body;
    data.imageID = response ? response.id : 0
    const campaignEntryResponse = await CampaignEntryLogic.createCampaignEntry(data, user);
    return campaignEntryResponse;

};
module.exports = CampaignEntryLogic