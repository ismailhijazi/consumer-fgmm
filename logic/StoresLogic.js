const { Stores, Images, RecipeStores, ProductStores } = require('../models/Index');
const methods = require('../helpers/methods')

class StoresLogic {

}

StoresLogic.getAllStores = async() => {
    return await Stores.findAll({
        attributes: ['id', 'name', 'description', 'location'],
        order: [
            ['updated_at', 'DESC']
        ],
        include: [{
            model: Images,
            attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
        }]
    });
};

StoresLogic.getStoresByID = async(id) => {
    return await Stores.findByPk(id, { 
        attributes: ['id', 'name', 'description', 'location'],
        include: [{
            model: Images,
            attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
        }]
    });
};

StoresLogic.createStores = async(data) => {
    let item = {
        name: data.name,
        description: data.description,
        location: data.location, 
        fk_image_id: data.imageID
    }
    if(data.id){
        item.updated_by = data.userID
    } else {
        item.created_by = data.userID
        item.updated_by = data.userID
    }
    
    return await methods.updateOrCreate(Stores, data.id, item)
};


StoresLogic.deleteStores = async(id) => {
    return await Stores.destroy({
        where: {
            id
        }
    })
};

StoresLogic.getAllStoresName = async() => {
    return await Stores.findAll({
        attributes: ['id', 'name'],
        order: [
            ['name', 'DESC']
        ]
    });
};


StoresLogic.deleteMultipleStores = async(data) => {
    const promises =  data.map(async(id) => {
        const allStores = await Stores.findAll({
            where: {
              id
            },
            include: [RecipeStores, ProductStores],
          });
        console.log(allStores)
        if(allStores[0].dataValues && (
            (allStores[0].dataValues.ProductStores && allStores[0].dataValues.ProductStores.length === 0)
        && (allStores[0].dataValues.RecipeStores && allStores[0].dataValues.RecipeStores.length === 0))) {
            await Stores.destroy({
                where: {
                    id
                }
            });
            return "Stores with ID: "+id+" is deleted."
        } else {
            return "Stores with ID: "+id+" is associated with Recipes Or Products, Can't delete."
        }
    })
    return Promise.all(promises)
};


module.exports = StoresLogic