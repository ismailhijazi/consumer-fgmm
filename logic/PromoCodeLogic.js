const multer = require('multer');
const fs = require('fs');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
const {
    CampaignEntry,
    PromoCodes,
    Campaigns,
    PromoFiles
} = require('../models/Index');
const {
    Op
} = require('sequelize');
const methods = require('../helpers/methods');

class PromoCodeLogic {

}

PromoCodeLogic.createPromoCode = async(data) => {
    const item = {
        title: data.title,
        description: data.description,
        fk_campaign_id: data.campaignID,
        code: data.code,
        start_date: new Date(data.start_date),
        end_date: new Date(data.end_date),
        created_by: data.userID
    };
    return await methods.updateOrCreate(PromoCodes, data.id, item)
};

PromoCodeLogic.getAllActivePromos = async() => {
    return await PromoCodes.findAll({
        where: {
            start_date: {
                [Op.lte]: new Date()
            },
            end_date: {
                [Op.gte]: new Date()
            }
        },
        order: [
            ['updated_at', 'DESC']
        ]
    });
};

PromoCodeLogic.getPromoWithCampaign = async(id) => {
    return await PromoCodes.findByPk(id, { include: [Campaigns] });
};

PromoCodeLogic.validatePromoCode = async(code, campaignID) => {
    // TODO:
    // Check if promo is exists (not some random sequence of characters)
    // Check if promo is valid
    // Check if promo is already used
    
    return await PromoCodes.findAll({
        where: {
            [Op.and]: [{
                code: code,
                fk_campaign_id:campaignID
            }, {
                start_date: {
                    [Op.lte]: new Date()
                },
                end_date: {
                    [Op.gte]: new Date()
                }
            }]
        },
        include: [CampaignEntry]
    });
};


PromoCodeLogic.uploadPromoCode = async(req,res, user) => {
    return new Promise(async(resolve, reject) => {
        let filename, filepath;
        const storage = multer.diskStorage({

            destination: function(req, file, cb) {
                const ext = file.mimetype.split('/')[1];
                var folderName = 'promo-code'
                filepath = "./public/"+folderName;
                if (fs.existsSync(filepath)) {
                    // file exists
                } else {
                    fs.mkdirSync(filepath);
                }

                cb(null, filepath);
            },
            filename: function(req, file, cb) {
                let ext = file.mimetype.split('/')[1];
                if(ext === 'vnd.ms-excel')    
                    ext = 'xls'
                if(ext === 'vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    ext = 'xls'
                filename = (new Date()).getTime() + "-" + user.id + "." + ext;
                cb(null, filename);
            }
        });
        var exceltojson; //Initialization

        let upload = await multer({ storage: storage }).array("file", 10);
        upload(req, res, async function (err) {
            if (err){
              res.status(400).send("fail saving image");
            } else {
                
                let reqfiles=res.req.files;
                const promises = []
                
                Promise.all(reqfiles.map(async function(element, index ){
                    const campaignID = res.req.body.fk_campaign_id;
                    await PromoFiles.create({
                        file_name: element.filename,
                        folder_path: element.destination.replace("./public", ""),
                        link: element.destination.replace("./public", "") + "/" + element.filename,
                        fk_campaign_id: campaignID,
                        created_by: user.id,
                    });
                    if(element.originalname.split('.')[element.originalname.split('.').length-1] === 'xlsx'){
                        exceltojson = xlsxtojson;
                    } else {
                        exceltojson = xlstojson;
                    }
                    try {
                        return new Promise(function(resolve, reject) {
                            exceltojson({
                                input: element.path, //the same path where we uploaded our file
                                output: null, //since we don't need output.json
                                lowerCaseHeaders:true
                            }, async function(err,result){
                                if(err) {
                                    reject(err);
                                }
                                const allCampaign = await Campaigns.findByPk(campaignID);

                                let promo = []
                                result.forEach((element,index) => {
                                    promo.push({
                                        title: allCampaign.title+ ' - '+element.code,
                                        description: allCampaign.description,
                                        fk_campaign_id: campaignID,
                                        code: element.code,
                                        start_date: allCampaign.start_date,
                                        end_date: allCampaign.end_date,
                                        created_by: user.id,
                                        created_at: new Date()
                                    })
                                });
                                PromoCodes.bulkCreate(promo);
                                resolve(element.originalname+" file inserted");

                            });
                        });
                    } catch (e){
                        return ("Corupted excel file");
                    }
                   
                })).then(function(values) {
                    resolve(values)
                });         
            }
          });
    });
}
module.exports = PromoCodeLogic