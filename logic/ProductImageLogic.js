const {
    Op
} = require('sequelize');
const { ProductImages } = require('../models/Index');
const methods = require('../helpers/methods');

class ProductImageLogic {

}

ProductImageLogic.createProductImages = async(data, userID) => {
    
    data.forEach(async element => {
        const item = {
            fk_image_id: element.fk_image_id,
            fk_product_id	: element.fk_product_id,
            created_by: userID,
            updated_by: userID
        };

        await methods.updateOrCreate(ProductImages, data.id, item)
    });
    
};

ProductImageLogic.getAllProductImages = async(id) => {
    return await ProductImages.findAll({
        where: {
            fk_product_id: id
        },
        order: [
            ['updated_at', 'DESC']
        ]
    });
};
module.exports = ProductImageLogic