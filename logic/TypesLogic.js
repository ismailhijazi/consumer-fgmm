const { Types, Images, Recipes, Products } = require('../models/Index');
const methods = require('../helpers/methods');
const e = require('express');
const ImageLogic = require('./ImageLogic')

class TypesLogic {

}

TypesLogic.getAllTypes = async() => {
    return await Types.findAll({
        attributes: ['id', 'category_id', 'description', 'name','active', 'fk_image_id'],
        include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
        }],
        order: [
            ['updated_at', 'DESC']
        ]
    });
};

TypesLogic.getTypesByID = async(id) => {
    return await Types.findByPk(id, { 
        attributes: ['id', 'category_id', 'description', 'name','active', 'fk_image_id'],
        include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
        }],
     });
};

TypesLogic.createTypes = async(data) => {
    if(data.removeImages) {
        let removeImagesData = JSON.parse(data.removeImages)
        data.removeImages = removeImagesData
        if(removeImagesData && removeImagesData.length > 0){
            await ImageLogic.deleteImages(data, data.userID)
        }
    }
    let item = {
        name: data.name,
        description: data.description,
        category_id: data.category_id, 
        fk_image_id: data.imageID
    }
    if(data.id){
        item.updated_by = data.userID
    } else {
        item.created_by = data.userID
        item.updated_by = data.userID
    }
    
    return await methods.updateOrCreate(Types, data.id, item)
};


TypesLogic.deleteTypes = async(id) => {
    return await Types.destroy({
        where: {
            id
        }
    })
};

TypesLogic.getAllTypesName = async() => {
    return await Types.findAll({
        attributes: ['id', 'name'],
        include: [{
            model: Images,
            attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
        }],
        order: [
            ['name', 'DESC']
        ]
    });
};

TypesLogic.deleteMultipleTypes = async(data) => {
    // var promises = []
    const promises =  data.map(async(id) => {
        const allTypes = await Types.findAll({
            where: {
              id
            },
            include: [Recipes, Products],
          });
        if(allTypes[0].dataValues && (
            (allTypes[0].dataValues.Products && allTypes[0].dataValues.Products.length === 0)
        && (allTypes[0].dataValues.Recipes && allTypes[0].dataValues.Recipes.length === 0))) {
            await Types.destroy({
                where: {
                    id
                }
            });
            return "Category with ID: "+id+" is deleted."
        } else {
            return "Category with ID: "+id+" is associated with Recipes Or Products, Can't delete."
        }
    })
    return Promise.all(promises)
};

module.exports = TypesLogic