const UsersLogic = require('./UsersLogic');
const CampaignEntryLogic = require('./CampaignEntryLogic');
const PromoCodeLogic = require('./PromoCodeLogic');
const CampaignLogic = require('./CampaignLogic');
const ImageLogic = require('./ImageLogic');
const NewsLogic = require('./NewsLogic');
const ProductLogic = require('./ProductLogic');
const SupportTicketLogic = require('./SupportTicketLogic');
const StatesLogic = require('./StatesLogic');
const CampaignImageLogic = require('./CampaignImageLogic');
const StoresLogic = require('./StoresLogic');
const TypesLogic = require('./TypesLogic');
const ProductImageLogic = require('./ProductImageLogic');
const ProductStoreLogic = require('./ProductStoreLogic');
const ItemCategoryLogic = require('./ItemCategoryLogic');
const RecipeLogic = require('./RecipeLogic');
const RecipeStoreLogic = require('./RecipeStoreLogic');
const RecipeProductLogic = require('./RecipeProductLogic');
const AppLogic = require('./AppLogic');
const UsersRecipeLogic = require('./UsersRecipeLogic');
const RecipeReviewsLogic = require('./RecipeReviewsLogic');



module.exports = {
    UsersLogic,
    CampaignEntryLogic,
    PromoCodeLogic,
    CampaignLogic,
    ImageLogic,
    NewsLogic,
    ProductLogic,
    SupportTicketLogic,
    StatesLogic,
    CampaignImageLogic,
    StoresLogic,
    TypesLogic,
    ProductImageLogic,
    ProductStoreLogic,
    ItemCategoryLogic,
    RecipeLogic,
    RecipeStoreLogic,
    AppLogic,
    RecipeProductLogic,
    UsersRecipeLogic,
    RecipeReviewsLogic
}