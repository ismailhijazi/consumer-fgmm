const { Campaigns, CampaignImages, Recipes, Images,
    Products, ProductImages,RecipeMedia, News } = require('../models/Index');
const methods = require('../helpers/methods');

const {
    Op
} = require('sequelize');

class AppLogic {

}

AppLogic.HomePageLogic = async() => {
    const campaigns = await AppLogic.getHomePageActiveCampaigns();
    const products = await AppLogic.getAllFeaturedProducts();
    const recipes = await AppLogic.getHomePageRecipes();
    const news = await AppLogic.getHomePageNews();
    return {
        campaigns,
        products,
        recipes,
        news
    }
}

AppLogic.getHomePageActiveCampaigns = async() => {
    const activeCampaigns = await Campaigns.findAll({
        where: {
            start_date: {
                [Op.lte]: new Date()
            },
            end_date: {
                [Op.gte]: new Date()
            }
        },
        attributes: ['id', 'title'],
        order: [
            ['updated_at', 'DESC']
        ],
        include: [ {
            model: CampaignImages,
            attributes: ['id'],
            include: [Images]
        }]
    })
    let allCampaigns = []
    activeCampaigns.forEach(activeCampaign => {
        let campaign = {
            id: activeCampaign.id,
            title: activeCampaign.title
        }
        
        if(activeCampaign.CampaignImages && activeCampaign.CampaignImages.length > 0){
            campaign.image_name = activeCampaign.CampaignImages[0].Image.image_name;
            campaign.image_path = activeCampaign.CampaignImages[0].Image.link;
            campaign.type = activeCampaign.CampaignImages[0].Image.type;
            campaign.thumbnail = activeCampaign.CampaignImages[0].Image.thumbnail;
        } else {
            campaign.image_name = ''
            campaign.image_path = ''
            campaign.type = ''
            campaign.thumbnail = ''
        }
        allCampaigns.push(campaign)
    });
    return allCampaigns
};

AppLogic.getAllFeaturedProducts = async() => {
    const allProducts = await Products.findAll({
        where: {
            is_feature: 1
        },
        attributes: ['id', 'name', 'default_image'],
        order: [
            ['updated_at', 'DESC']
        ],
        include: [ {
            model: ProductImages,
            include: [Images]
        }]
    });

    
    let allFeaturedProducts = []
    allProducts.forEach(data => {
        let featureProducts = {
            id: data.id,
            title: data.name,
            image_name : '',
            image_path:  '',
            type :'',
            thumbnail: ''
        }
        if(data.ProductImages && data.ProductImages.length > 0){
            data.ProductImages.forEach(image => {
                if(data.default_image === image.fk_image_id){
                    featureProducts.image_name = image.Image.image_name
                    featureProducts.image_path=  image.Image.link
                    featureProducts.type = image.Image.type;
                    featureProducts.thumbnail = image.Image.thumbnail;
                }
            })

            allFeaturedProducts.push(featureProducts)
        }
    });
    return allFeaturedProducts
};

AppLogic.getHomePageRecipes = async() => {
    const allRecipes = await Recipes.findAll({
        where: {
            active: 1
        },
        attributes: ['id', 'title'],
        order: [
            ['id', 'DESC']
        ],
        include: [ {
            model: RecipeMedia,
            attributes: ['id'],
            include: [Images]
        }],
        limit: 2
    });
    let recipesData = []
    allRecipes.forEach(data => {
        let allRecipesData = {
            id: data.id,
            title: data.title
        }

        if(data.RecipeMedia && data.RecipeMedia.length > 0){
            allRecipesData.image_name = data.RecipeMedia[0].Image.image_name
            allRecipesData.image_path = data.RecipeMedia[0].Image.link
            allRecipesData.type = data.RecipeMedia[0].Image.type;
            allRecipesData.thumbnail = data.RecipeMedia[0].Image.thumbnail;
        } else {
            allRecipesData.image_name = ''
            allRecipesData.image_path = ''
            allRecipesData.type = ''
            allRecipesData.thumbnail = ''
        }
        recipesData.push(allRecipesData)
    });
    return recipesData
};


AppLogic.getHomePageNews = async() => {
    const allNews = await News.findAll({
        where: {
            publish_status: 1,
            is_feature: true
        },
        attributes: ['id', 'title', 'start_date'],
        order: [
            ['id', 'DESC']
        ],
        include: [Images],
        limit: 2
    });
    let NewsData = []
    allNews.forEach(data => {
        let allNewsData = {
            id: data.id,
            title: data.title
        }

        if(data.Image ){
            allNewsData.image_name = data.Image.image_name
            allNewsData.image_path = data.Image.link
            allNewsData.type = data.Image.type
            allNewsData.thumbnail = data.Image.thumbnail
        } else {
            allNewsData.image_name = ''
            allNewsData.image_path = ''
            allNewsData.type = ''
            allNewsData.thumbnail = ''
        }
        NewsData.push(allNewsData)
    });
    return NewsData
};

AppLogic.getAppDetail = async() => {
    return 0;
};

module.exports = AppLogic