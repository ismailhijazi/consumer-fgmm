const { States } = require('../models/Index');

class StatesLogic {

}

StatesLogic.getAllStates = async(categoryID) => {
    return [
        "Al Anbar",
        "Babil",
        "Baghdad",
        "Basra",
        "Dhi Qar",
        "Al-Qādisiyyah",
        "Diyala",
        "Duhok",
        "Erbil",
        "Halabja",
        "Karbala",
        "Kirkuk",
        "Maysan",
        "Muthanna",
        "Najaf",
        "Nineveh",
        "Saladin",
        "Sulaymaniyah (Slêmanî)",
        "Wasi"
    ]
};

module.exports = StatesLogic