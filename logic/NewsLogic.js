const { News, Images } = require('../models/Index');
const methods = require('../helpers/methods')
const ImageLogic = require('./ImageLogic')

class NewsLogic {

}

NewsLogic.publishedNews = async() => {
    return await News.findAll({
        where: {
            publish_status: 1
        },
        order: [
            ['updated_at', 'DESC']
        ],
        include: [Images]
    });
};

NewsLogic.getAllNews = async() => {
    return await News.findAll({
        order: [
            ['updated_at', 'DESC']
        ],
        include: [Images]
    });
};

NewsLogic.getNewsByID = async(id) => {
    return await News.findByPk(id, { include: [Images] });
};

NewsLogic.createNews = async(data) => {
    if(data.removeImages) {
        let removeImagesData = JSON.parse(data.removeImages)
        data.removeImages = removeImagesData
        if(removeImagesData && removeImagesData.length > 0){
            await ImageLogic.deleteImages(data, data.userID)
        }
    }
    const item = {
        title: data.title,
        description: data.description,
        publish_status: data.publish_status, // 1 - draft, 2 - published, 3 - expired
        fk_image_id: data.imageID,
        start_date: new Date(data.start_date),
        end_date: new Date(data.end_date),
        is_feature: data.is_feature
    }
    return await methods.updateOrCreate(News, data.id, item)
};

NewsLogic.getFeatureNews = async() => {
    return await News.findAll({
        where: {
            publish_status: 2,
            is_feature: true
        },
        order: [
            ['updated_at', 'DESC']
        ],
        include: [Images]
    });
};

NewsLogic.deleteNews = async(id) => {
    return await News.destroy({
        where: {
            id
        }
    })
};
NewsLogic.deleteMultipleNews = async(data) => {
    var promises = []
    promises.push(data.forEach( async(id) => {
        return await News.destroy({
            where: {
                id
            }
        })
    }));
    return Promise.all(promises)
};

module.exports = NewsLogic