const {  Recipes, RecipeMedia, Types, RecipeStores,RecipeReviews, Images, Stores, RecipeProducts, Products, ProductImages, UsersRecipe,Users } = require('../models/Index');
const RecipeStoreLogic = require('./RecipeStoreLogic')
const RecipeImageLogic = require('./RecipeImageLogic')
const ImageLogic = require('./ImageLogic')
const RecipeProductLogic = require('./RecipeProductLogic')
const methods = require('../helpers/methods')

class RecipeLogic {

}

RecipeLogic.getAllRecipes = async() => {
    return await Recipes.findAll({
        order: [
            ['updated_at', 'DESC']
        ],
        include: [{
            model: Types,
            attributes: ['id', 'category_id', 'description', 'name','active'],
        }, {
            model: RecipeMedia,
            attributes: ['id'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: RecipeStores,
            attributes: ['id'],
            include: [{
                model: Stores,
                attributes: ['id', 'name', 'description', 'location'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }]
        }, {
            model: RecipeProducts,
            attributes: ['id'],
            include: [{
                model: Products,
                attributes: ['id', 'name', 'description', 'weight','pieces',
                'product_type_id', 'is_feature', 'default_image'],
                include: [{
                    model: ProductImages,
                    attributes: ['id'],
                    include: [{
                        model: Images,
                        attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                    }]
                }]
            }]
        }]
    });
};

RecipeLogic.getRecipeByID = async(id) => {
    return await Recipes.findByPk(id, { include: 
        [{
            model: Types,
            attributes: ['id', 'category_id', 'description', 'name','active']
        }, {
            model: RecipeMedia,
            attributes: ['id'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: RecipeStores,
            attributes: ['id'],
            include: [{
                model: Stores,
                attributes: ['id', 'name', 'description', 'location'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }]
        }, {
            model: RecipeProducts,
            attributes: ['id'],
            include: [{
                model: Products,
                attributes: ['id', 'name', 'description', 'weight','pieces',
                'product_type_id', 'is_feature', 'default_image'],
                include: [{
                    model: ProductImages,
                    attributes: ['id'],
                    include: [{
                        model: Images,
                        attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                    }]
                }]
            }]
        }, {
            model: RecipeReviews,
            attributes: ['id', 'rating','description','review_date'],
            include: [{
                    model: Users,
                    attributes: ['id', 'first_name', 'middle_name', 'last_name']
            }]
        }, {
            model: UsersRecipe,
            attributes: ['id', 'fk_recipe_id','fk_user_id']
        }]
    });
};


RecipeLogic.createRecipe = async(data) => {
    if(data.files && data.files > 0){

    }
    let item = {
        title: data.title,
        description: data.description,
        fk_type_id: data.fk_type_id, 
        persons: data.persons,
        estimated_time: data.estimated_time,
        ingredients: data.ingredients,
        active: 1
    }
    if(data.id){
        item.updated_by = data.userID
    } else {
        item.created_by = data.userID
        item.updated_by = data.userID
    }
    const data1 = await methods.updateOrCreate(Recipes, data.id, item);
    if(data.stores && data1.item){
        let recipeStores = []
        let storeData = JSON.parse(data.stores)
        storeData.forEach(element => {
            recipeStores.push ({
                fk_recipe_id: data1.item.dataValues.id,
                fk_store_id: element.id
            })
        });
       await RecipeStoreLogic.createRecipeStores(recipeStores, data.userID)
    }
    if(data.recipeImages && data1.item){
        let recipeImages = []
        let recipeImagesData = JSON.parse(data.recipeImages)
        recipeImagesData.forEach(element => {
            recipeImages.push ({
                fk_recipe_id: data1.item.dataValues.id,
                fk_image_id: element
            })
        });
        
       await RecipeImageLogic.createRecipeImage(recipeImages, data.userID)
    }
    if(data.products && data1.item){
        let recipeProducts = []
        let productsData = JSON.parse(data.products)
        productsData.forEach(element => {
            recipeProducts.push ({
                fk_recipe_id: data1.item.dataValues.id,
                fk_product_id: element.id
            })
        });
       await RecipeProductLogic.createRecipeProducts(recipeProducts, data.userID)
    }
    if(data.removeImages) {
        let removeImagesData = JSON.parse(data.removeImages)
        data.removeImages = removeImagesData
        if(removeImagesData && removeImagesData.length > 0 && data1.item){
        await ImageLogic.deleteImages(data, data.userID)
        }
    }
    return data1
    
};



RecipeLogic.deleteMultipleRecipes = async(data) => {
    const promises =  data.map(async(id) => {
        await RecipeMedia.destroy({
            where: {
                fk_recipe_id: id
            }
        })
        await RecipeStores.destroy({
            where: {
                fk_recipe_id: id
            }
        })
        await RecipeProducts.destroy({
            where: {
                fk_recipe_id: id
            }
        })
        await RecipeReviews.destroy({
            where: {
                fk_recipe_id: id
            }
        })
        await UsersRecipe.destroy({
            where: {
                fk_recipe_id: id
            }
        })
        return await Recipes.destroy({
            where: {
                id
            }
        })
    });
    return Promise.all(promises)
};

RecipeLogic.getRecipeByCategory = async(categoryID) => {
    return await Recipes.findAll({
        attributes: ['id', 'title', 'description', 'estimated_time','persons',
        'ingredients'],
        where: {
            fk_type_id: categoryID
        },
        order: [
            ['updated_at', 'DESC']
        ],
        include: 
            [{
                model: Types,
                attributes: ['id', 'category_id', 'description', 'name','active']
            }, {
                model: RecipeMedia,
                attributes: ['id'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }, {
                model: RecipeStores,
                attributes: ['id'],
                include: [{
                    model: Stores,
                    attributes: ['id', 'name', 'description', 'location'],
                    include: [{
                        model: Images,
                        attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                    }]
                }]
            }, {
                model: RecipeProducts,
                attributes: ['id'],
                include: [{
                    model: Products,
                    attributes: ['id', 'name', 'description', 'weight','pieces',
                    'product_type_id', 'is_feature', 'default_image'],
                    include: [{
                        model: ProductImages,
                        attributes: ['id'],
                        include: [{
                            model: Images,
                            attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                        }]
                    }]
                }]
            }]
    });
};
module.exports = RecipeLogic