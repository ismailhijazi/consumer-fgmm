const multer = require('multer');
const fs = require('fs');
const { Images, CampaignImages, ProductImages, RecipeMedia } = require('../models/Index');
const thumbanailUrl = "http://consumer.api.fgmmdev.com"
class ImageLogic {

}
ImageLogic.addImage = async(data, user) => {
    if (data.image_name && data.folder_path && data.link)
        return await Images.create({
            image_name: data.image_name,
            folder_path: data.folder_path,
            link: data.link,
            type: data.type,
            thumbnail: data.thumbnail,
            created_by: user.username,
            updated_by: user.username,
            module_id: data.module_id
        });
    else
        return ''
};

ImageLogic.uploadImage = async(req,res, user) => {
    return new Promise(async(resolve, reject) => {
        let imageCategory, filename, filepath;
        const storage = multer.diskStorage({

            destination: function(req, file, cb) {
                const ext = file.mimetype.split('/')[1];
                var folderName = 'videos'
                if(ext === 'jpeg' || ext === 'jpg' || ext === 'png')
                    folderName = 'images'
                imageCategory = req.body.category || "default";
                filepath = "./public/"+folderName+"/" + imageCategory;
                if (fs.existsSync(filepath)) {
                    // file exists
                } else {
                    fs.mkdirSync(filepath);
                }

                cb(null, filepath);
            },
            filename: function(req, file, cb) {
                const ext = file.mimetype.split('/')[1];
                filename = (new Date()).getTime() + "-" + user.id + "." + ext;
                cb(null, filename);
            }
        });

        let upload = await multer({ storage: storage }).fields(
            [
              { 
                name: 'file', 
                maxCount: 50 
              }, 
              { 
                name: 'thumbnailFile', 
                maxCount: 50 
              }
            ]
          );
          

        upload(req, res, async function (err) {
            console.log(err, 'error')
            if (err){
              res.status(400).send("fail saving image");
            } else {
                
                let reqfiles = res.req.files.file;
                let thumbnailFiles = res.req.files.thumbnailFile;
                let thumbnailIndex = req.body.thumbnailIndex;
                let thumbnailFilesIndex = req.body.thumbnailFilesIndex;

                const promises = []
                
                let thumbIndex = '';
                let thumbFilesIndex = '';
                if(thumbnailIndex)
                    thumbIndex = thumbnailIndex.split(',')
                
                if(thumbnailFilesIndex)
                    thumbFilesIndex = thumbnailFilesIndex.split(',')
                
                reqfiles.forEach(async( element, index )=> {
                    
                    const indexData = (thumbIndex)?thumbIndex.filter(data => parseInt(data) ===  index): '';
                    let thumbFile = ''
                    if(indexData.length > 0){
                        thumbFile = thumbnailFiles[thumbFilesIndex[0]]
                        thumbFilesIndex.splice(0, 1);
                    }

                    promises.push(
                         new Promise((resolve, reject) => {
                            resolve(ImageLogic.addImage({
                                image_name: element.filename,
                                folder_path: element.destination.replace("./public", ""),
                                link: element.destination.replace("./public", "") + "/" + element.filename,
                                module_id: req.body.module_id,
                                type: element.destination.includes("images")?"image":"video",
                                thumbnail: (indexData.length > 0)?thumbanailUrl+thumbFile.destination.replace("./public", "") + "/" + thumbFile.filename: ""
                            }, req.user))
                        })
                    )
                })
                
                Promise.all(promises)
                .then(response => res.send(response))
              
            }
          });
    });
}

ImageLogic.getAllImagesUploadedByUser = async(user) => {
    let images = await Images.findAll({
        where: {
            fk_user_id: user.id
        },
        order: [
            ['updated_at', 'DESC']
        ]
    });
    return images;
};

ImageLogic.deleteImages = async(data, user) => {
    if (data && data.removeImages){
        data.removeImages.forEach(imageId => {
            if(data.category_name === 'campaign'){
                CampaignImages.destroy({
                    where: {
                        fk_image_id: imageId
                    }
                })
            } 
            if(data.category_name === 'product'){
                ProductImages.destroy({
                    where: {
                        fk_image_id: imageId
                    }
                })
            }
            if(data.category_name === 'recipe'){
                RecipeMedia.destroy({
                    where: {
                        fk_image_id: imageId
                    }
                })
            }
            Images.destroy({
                where: {
                    id: imageId
                }
            })
        });
    }
        
    else
        return ''
};
module.exports = ImageLogic