const {
    Op
} = require('sequelize');
const { CampaignImages } = require('../models/Index');
const methods = require('../helpers/methods');

class CampaignImageLogic {

}

CampaignImageLogic.createCampaignImages = async(data, userID) => {
    data.forEach(async element => {
        const item = {
            fk_image_id: element.fk_image_id,
            fk_campaign_id	: element.fk_campaign_id,
            created_by: userID,
            updated_by: userID
        };

        await methods.updateOrCreate(CampaignImages, data.id, item)
    });
    
};

module.exports = CampaignImageLogic