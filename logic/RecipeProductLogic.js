const {
    Op
} = require('sequelize');
const { RecipeProducts } = require('../models/Index');
const methods = require('../helpers/methods');

class RecipeProductLogic {

}

RecipeProductLogic.createRecipeProducts = async(data, userID) => {
    RecipeProducts.destroy({
        where: {
           fk_recipe_id: data[0].fk_recipe_id
        }
    })
    data.forEach(async element => {
        const item = {
            fk_product_id: element.fk_product_id,
            fk_recipe_id	: element.fk_recipe_id,
            created_by: userID,
            updated_by: userID
        };

        await methods.updateOrCreate(RecipeProducts, data.id, item)
    });
    
};

module.exports = RecipeProductLogic