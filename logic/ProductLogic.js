const { Products, ProductImages, Types, ProductStores, Images, Stores,RecipeMedia, RecipeProducts,Recipes } = require('../models/Index');
const methods = require('../helpers/methods')
const ProductImageLogic = require('./ProductImageLogic')
const ProductStoreLogic = require('./ProductStoreLogic')
const ImageLogic = require('./ImageLogic')

class ProductLogic {

}

ProductLogic.getProductByCategory = async(categoryID) => {
    return await Products.findAll({
        attributes: ['id', 'name', 'description', 'weight','pieces',
        'product_type_id', 'is_feature', 'default_image', 'types_id'],
        where: {
            types_id: categoryID
        },
        order: [
            ['updated_at', 'DESC']
        ],
        include: [{
            model: Types,
            attributes: ['id', 'category_id', 'description', 'name','active'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: ProductImages,
            attributes: ['id'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: ProductStores,
            attributes: ['id'],
            include: [{
                model: Stores,
                attributes: ['id', 'name', 'description', 'location'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }]
        }]
    });
};
ProductLogic.getAllProducts = async() => {
    return await Products.findAll({
        attributes: ['id', 'name', 'description', 'weight','pieces',
        'product_type_id', 'is_feature', 'default_image', 'types_id'],
        order: [
            ['updated_at', 'DESC']
        ],
        include: [{
            model: Types,
            attributes: ['id', 'category_id', 'description', 'name','active'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: ProductImages,
            attributes: ['id'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: RecipeProducts,
            attributes: ['id'],
            include: [{
                model: Recipes,
                attributes:  ['id', 'title', 'description', 'estimated_time','persons',
                'ingredients'],
                include: [{
                    model: RecipeMedia,
                    attributes: ['id'],
                    include: [{
                        model: Images,
                        attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                    }]
                }]
            }]
        }]
    });
};

ProductLogic.getProductByID = async(id) => {
    return await Products.findByPk(id, { 
    attributes: ['id', 'name', 'description', 'weight','pieces',
        'product_type_id', 'is_feature', 'default_image', 'types_id'],
        include: [{
            model: Types,
            attributes: ['id', 'category_id', 'description', 'name','active'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: ProductImages,
            attributes: ['id'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: ProductStores,
            attributes: ['id'],
            include: [{
                model: Stores,
                attributes: ['id', 'name', 'description', 'location'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }]
        }, {
            model: RecipeProducts,
            attributes: ['id'],
            include: [{
                model: Recipes,
                attributes:  ['id', 'title', 'description', 'estimated_time','persons',
                'ingredients'],
                include: [{
                    model: RecipeMedia,
                    attributes: ['id'],
                    include: [{
                        model: Images,
                        attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                    }]
                }]
            }]
        }]
    });
};


ProductLogic.createProducts = async(data) => {
    let item = {
        name: data.name,
        description: data.description,
        weight: data.weight, 
        pieces: data.pieces,
        types_id: data.types_id, 
        product_type_id: data.product_type_id,
        item_code: data.item_code, 
        item_name: data.item_name,
        item_group_code: data.item_group_code, 
        item_group_name: data.item_group_name,
        canceled: data.item_group_name,
        default_image: data.imageID,
        active: 1,
        is_feature: data.is_feature
    }
    if(data.id){
        item.updated_by = data.userID
    } else {
        item.created_by = data.userID
        item.updated_by = data.userID
    }
    
    let data1 = await methods.updateOrCreate(Products, data.id, item);
    if(data.productImages && data1.item){
        let productImages = []
        let productImagesData = JSON.parse(data.productImages)
        productImagesData.forEach(element => {
            productImages.push ({
                fk_product_id: data1.item.dataValues.id,
                fk_image_id: element
            })
        });
        
       await ProductImageLogic.createProductImages(productImages, data.userID)
    }

    if(data.stores && data1.item){
        let productStores = []
        let storeData = JSON.parse(data.stores)
        storeData.forEach(element => {
            productStores.push ({
                fk_product_id: data1.item.dataValues.id,
                fk_store_id: element.id
            })
        });
       await ProductStoreLogic.createProductStores(productStores, data.userID)
    }

   
    let allImages = await ProductImageLogic.getAllProductImages(data1.item.dataValues.id)
    if(allImages.length > 0){
        let item1 = {
            default_image: allImages[0].dataValues.fk_image_id
        }
        await methods.updateOrCreate(Products, data1.item.dataValues.id, item1)
    }
    return data1;
};


ProductLogic.deleteMultipleProducts = async(data) => {
    var promises = []
    promises.push(data.forEach( async(id) => {
        await ProductImages.destroy({
            where: {
                fk_product_id: id
            }
        })
        await ProductStores.destroy({
            where: {
                fk_product_id: id
            }
        })
        await RecipeProducts.destroy({
            where: {
                fk_product_id: id
            }
        })
        return await Products.destroy({
            where: {
                id
            }
        })
    }));
    return Promise.all(promises)
};

ProductLogic.getAllFeaturedProducts = async() => {
    return await Products.findAll({
        where: {
            is_feature: 1
        },
        attributes: ['id', 'name', 'description', 'weight','pieces',
        'product_type_id', 'is_feature', 'default_image', 'types_id'],
        order: [
            ['updated_at', 'DESC']
        ],
        include: [{
            model: Types,
            attributes: ['id', 'category_id', 'description', 'name','active'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: ProductImages,
            attributes: ['id'],
            include: [{
                model: Images,
                attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
            }]
        }, {
            model: ProductStores,
            attributes: ['id'],
            include: [{
                model: Stores,
                attributes: ['id', 'name', 'description', 'location'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }]
        }, {
            model: RecipeProducts,
            attributes: ['id'],
            include: [{
                model: Recipes,
                attributes:  ['id', 'title', 'description', 'estimated_time','persons',
                'ingredients'],
                include: [{
                    model: RecipeMedia,
                    attributes: ['id'],
                    include: [{
                        model: Images,
                        attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                    }]
                }]
            }]
        }]
    });
};

ProductLogic.getAllProductName = async() => {
    return await Products.findAll({
        attributes: ['id', 'name'],
        order: [
            ['updated_at', 'DESC']
        ]
    });
};
module.exports = ProductLogic