const { UsersRecipe, Recipes, RecipeMedia, Images } = require('../models/Index');
const {
    Op
} = require('sequelize');
const methods = require('../helpers/methods');

class UsersRecipeLogic {

}

UsersRecipeLogic.getAllUsersRecipe = async(data) => {
    return await UsersRecipe.findAll({
        attributes: ['id', 'fk_recipe_id', 'fk_user_id'],
        where: {
            fk_user_id: data.userID
        },
        order: [
            ['updated_at', 'DESC']
        ],
        include: [{
            model: Recipes,
            attributes:  ['id', 'title', 'description', 'estimated_time','persons',
                'ingredients'],
            include: [{
                model: RecipeMedia,
                attributes: ['id'],
                include: [{
                    model: Images,
                    attributes: ['id', 'image_name', 'folder_path', 'link','module_id', 'type', 'thumbnail'],
                }]
            }]
        }]
    })
};

UsersRecipeLogic.createUsersRecipe = async(data) => {
    await UsersRecipe.destroy({
        where: {
            fk_recipe_id: data.recipe_id,
            fk_user_id: data.userID,
        }
    })
    if(data.favourite === true){
        const item = {
            fk_recipe_id: data.recipe_id,
            fk_user_id: data.userID,
            created_by: data.userID
        };
        return await methods.updateOrCreate(UsersRecipe, data.id, item);
    } else {
        return "Recipe Removed"
    }
};

UsersRecipeLogic.deleteUsersRecipe = async(data) => {
    var promises = []
    promises.push(data.forEach( async(id) => {
        return await UsersRecipe.destroy({
            where: {
                id
            }
        })
    }));
    return Promise.all(promises)
};
module.exports = UsersRecipeLogic