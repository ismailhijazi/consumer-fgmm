const { SupportTickets } = require('../models/Index');
const methods = require('../helpers/methods')

class SupportTicketLogic {

}

SupportTicketLogic.getSupportTicketsForUser = async(data) => {
    return await SupportTickets.findAll({
        where: {
            fk_user_id: data.userID
        },
        order: [
            ['updated_at', 'DESC']
        ]
    });
};

SupportTicketLogic.createSupportTicket = async(data) => {
    const item = {
        message: data.message,
        fk_user_id: data.userID,
        status: data.status || 0,
        created_by: data.userID
    };
    return await methods.updateOrCreate(SupportTickets, data.id, item)
};

SupportTicketLogic.getTicketByID = async(id) => {
    return await SupportTickets.findByPk(id);
};

module.exports = SupportTicketLogic