const {
    Op
} = require('sequelize');
const { RecipeStores } = require('../models/Index');
const methods = require('../helpers/methods');

class RecipeStoreLogic {

}

RecipeStoreLogic.createRecipeStores = async(data, userID) => {
    RecipeStores.destroy({
        where: {
           fk_recipe_id: data[0].fk_recipe_id
        }
    })
    data.forEach(async element => {
        const item = {
            fk_store_id: element.fk_store_id,
            fk_recipe_id	: element.fk_recipe_id,
            created_by: userID,
            updated_by: userID
        };

        await methods.updateOrCreate(RecipeStores, data.id, item)
    });
    
};

module.exports = RecipeStoreLogic