const methods = require('../helpers/methods')
const Controller = require('./Controller')
const { NewsLogic } = require('../logic');

class NewsController extends Controller {
    //
}

NewsController.getAllPublishedNews = async(req, res) => {
    const currentNews = await NewsLogic.publishedNews()
    res.send(methods.successResponse(
        'All News', {
            data: currentNews
        }
    ))
}
NewsController.getAllNews = async(req, res) => {
    const currentNews = await NewsLogic.getAllNews()
    res.send(methods.successResponse(
        'All News', {
            data: currentNews
        }
    ))
}

NewsController.createNews = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let news = await NewsLogic.createNews(data)
    res.send(methods.successResponse(
        data.id ? 'News Updated' : 'News Created', {
            data: news
        }
    ))
};

NewsController.getNewsByID = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const news = await NewsLogic.getNewsByID(id);
    res.send(methods.successResponse(
        'News by id', {
            data: news
        }
    ));
};

NewsController.getFeatureNews = async(req, res) => {
    const news = await NewsLogic.getFeatureNews();
    res.send(methods.successResponse(
        'News by id', {
            data: news
        }
    ));
};

NewsController.deleteNews = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const news = await NewsLogic.deleteNews(id);
    res.send(methods.successResponse(
        'News Deleted', {
            data: news
        }
    ));
};

NewsController.deleteMultipleNews = async(req, res) => {
    let data = req.body;
    const news = await NewsLogic.deleteMultipleNews(data);
    res.send(methods.successResponse(
        'News Deleted', {
            data: news
        }
    ));
};
module.exports = NewsController