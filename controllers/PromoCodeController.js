const methods = require('../helpers/methods');
const { CampaignEntryLogic, PromoCodeLogic } = require('../logic');
const Controller = require('./Controller')

class PromoCodeController extends Controller {
    //
}

PromoCodeController.createPromoCode = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let newPromoCode = await PromoCodeLogic.createPromoCode(data);
    res.send(methods.successResponse(
        data.id ? 'Promo code updated' : 'New promo code added', {
            data: newPromoCode
        }
    ))
};

PromoCodeController.getAllActivePromos = async(req, res) => {
    const activePromos = await PromoCodeLogic.getAllActivePromos()
    res.send(methods.successResponse(
        'Active promos', {
            data: activePromos
        }
    ))
};

PromoCodeController.getPromoByID = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const promo = await PromoCodeLogic.getPromoWithCampaign(id);
    res.send(methods.successResponse(
        'Get Promo Compaign code', {
            data: promo
        }
    ));
};

PromoCodeController.validatePromo = async(req, res) => {
    let code = req.params.code || req.body.promocode || 0;
    const promo = await PromoCodeLogic.validatePromoCode(code);

    res.send(methods.successResponse(
        'Validated Promo code', {
            data: promo
        }
    ));
};

PromoCodeController.applyPromo = async(req, res) => {
    let code = req.params.code || req.body.code || 0;
    let campaignID = req.params.campaignID || req.body.campaignID || 0;
    let promo = await PromoCodeLogic.validatePromoCode(code, campaignID);

    if (!promo || !promo.length) {
        throw Error("Invalid Promo Code");
    }

    promo = promo[0];
    const campaignEntry = await CampaignEntryLogic.createCampaignEntry({
        campaignID: promo.fk_campaign_id,
        promoID: promo.id
    }, req.user);

    res.send(methods.successResponse(
        'Validated Promo code', {
            data: campaignEntry
        }
    ));
};

PromoCodeController.getPreviouslyEnteredPromoCodes = async(req, res) => {
    const promos = await PromoCodeLogic.getPreviouslyEnteredPromoCodes(req.user);

    res.send(methods.successResponse(
        'success', {
            data: promos
        }
    ));
};

PromoCodeController.createPromoCodeByCampaign = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let newPromoCode = await PromoCodeLogic.uploadPromoCode(req, res, req.user);
    console.log(newPromoCode, 'new')
    res.send(methods.successResponse(
        data.id ? 'Promo code updated' : 'New promo code added', {
            data: newPromoCode
        }
    ))
};

module.exports = PromoCodeController