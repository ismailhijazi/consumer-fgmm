const methods = require('../helpers/methods')
const Controller = require('./Controller');
const { ImageLogic } = require('../logic');

class ImageController extends Controller {
    //
}

ImageController.getAllImagesUploadedByUser = async(req, res) => {
    const images = await ImageLogic.getAllImagesUploadedByUser(req.user);
    res.send(methods.successResponse(
        'All images uploaded by user', {
            data: images
        }
    ));
};

ImageController.uploadImage = async(req, res) => {

    const uploadResponse = await ImageLogic.uploadImage(req, res, req.user);
    res.send(methods.successResponse(
        'Uploaded image', {
            data: uploadResponse
        }
    ));
};

ImageController.deleteImages = async(req, res) => {
    let data = req.body;
    const deleteImagesResponse = await ImageLogic.deleteImages(data, req.user);
    res.send(methods.successResponse(
        'delete image', {
            data: deleteImagesResponse
        }
    ));
};
module.exports = ImageController