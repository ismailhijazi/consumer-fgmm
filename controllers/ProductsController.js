const methods = require('../helpers/methods')
const Controller = require('./Controller')
const { ProductLogic } = require('../logic');

class ProductsController extends Controller {
    //
}

ProductsController.getProductByCategories = async(req, res) => {
    let itemCategory = req.params.category || req.body.category || 0;
    const products = await ProductLogic.getProductByCategory(itemCategory);
    res.send(methods.successResponse(
        'Products', {
            data: products
        }
    ))
}
ProductsController.getAllProducts = async(req, res) => {
    const products = await ProductLogic.getAllProducts();
    res.send(methods.successResponse(
        'Products', {
            data: products
        }
    ))
}
ProductsController.getProductByID = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const product = await ProductLogic.getProductByID(id);
    res.send(methods.successResponse(
        'Products', {
            data: product
        }
    ))
}

ProductsController.createProducts = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let newProduct = await ProductLogic.createProducts(data);
    res.send(methods.successResponse(
        data.id ? 'Product Updated' : 'Product Created', {
            data: newProduct
        }
    ))
};

ProductsController.deleteMultipleProducts = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let newProduct = await ProductLogic.deleteMultipleProducts(data);
    res.send(methods.successResponse(
        'Product Deleted', {
            data: newProduct
        }
    ))
};
ProductsController.getAllFeaturedProducts = async(req, res) => {
    const products = await ProductLogic.getAllFeaturedProducts();
    res.send(methods.successResponse(
        'Products', {
            data: products
        }
    ))
}

ProductsController.getAllProductName = async(req, res) => {
    const currentProduct = await ProductLogic.getAllProductName()
    res.send(methods.successResponse(
        'All Products', {
            data: currentProduct
        }
    ))
}

module.exports = ProductsController