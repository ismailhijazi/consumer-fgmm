const methods = require('../helpers/methods')
const Controller = require('./Controller')
const { StoresLogic } = require('../logic');

class StoresController extends Controller {
    //
}

StoresController.getAllStores = async(req, res) => {
    const currentStores = await StoresLogic.getAllStores()
    res.send(methods.successResponse(
        'All Stores', {
            data: currentStores
        }
    ))
}

StoresController.createStores = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let stores = await StoresLogic.createStores(data)
    res.send(methods.successResponse(
        data.id ? 'Stores Updated' : 'Stores Created', {
            data: stores
        }
    ))
};

StoresController.getStoresByID = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const stores = await StoresLogic.getStoresByID(id);
    res.send(methods.successResponse(
        'Stores by id', {
            data: stores
        }
    ));
};

StoresController.deleteStores = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const stores = await StoresLogic.deleteStores(id);
    res.send(methods.successResponse(
        'News Deleted', {
            data: stores
        }
    ));
};

StoresController.getAllStoresName = async(req, res) => {
    const currentStores = await StoresLogic.getAllStoresName()
    res.send(methods.successResponse(
        'All Stores', {
            data: currentStores
        }
    ))
}

StoresController.deleteMultipleStores = async(req, res) => {
    let data = req.body;
    const stores = await StoresLogic.deleteMultipleStores(data);
    res.send(methods.successResponse(
        'Stores Deleted', {
            data: stores
        }
    ));
};
module.exports = StoresController