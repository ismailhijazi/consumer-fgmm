const jwt = require('jsonwebtoken');

const methods = require('../helpers/methods')
const Controller = require('./Controller')

const { UsersLogic } = require('../logic');

class UsersController extends Controller {
    //
}

UsersController.login = async(req, res) => {
    const user = await UsersLogic.validateUser(req.body);

    if (user) {
        let accessToken = jwt.sign({
            id: user.id,
            username: user.username,
        }, process.env.JWT_SECRET, { expiresIn: '365d' });

        res.header('Authorization', 'Bearer ' + accessToken);
        res.cookie('accessToken', accessToken, { maxAge: 90000000, httpOnly: true });
        res.json({
            status: true,
            accessToken,
            is_verified: true,
            user
        });
    } else {
        res.send(methods.errorResponse("Invalid credentials"));
    }
};

UsersController.adminlogin = async(req, res) => {
    const user = await UsersLogic.validateAdminUser(req.body);
    if (user) {
        let accessToken = jwt.sign({
            id: Math.floor(Math.random() * 90000) + 10000,
            username: user.username,
        }, process.env.JWT_SECRET, { expiresIn: '365d' });

        res.header('Authorization', 'Bearer ' + accessToken);
        res.cookie('accessToken', accessToken, { maxAge: 90000000, httpOnly: true });
        res.json({
            status: true,
            accessToken,
            name:  user.username,
            is_verified: true
        });
    } else {
        res.send(methods.errorResponse("Invalid credentials"));
    }
};
UsersController.create = async(req, res) => {
    const user = await UsersLogic.createUser(req.body);
    if(user && user.item.dataValues.id && req.body.is_app){
        let accessToken = jwt.sign({
            id: user.id,
            username: user.username,
        }, process.env.JWT_SECRET, {
            expiresIn: '8h'
        });

        res.header('Authorization', 'Bearer ' + accessToken);
        res.cookie('accessToken', accessToken, { maxAge: 900000, httpOnly: true });
        res.json({
            status: true,
            accessToken,
            is_verified: true,
            user
        });
    } else if(user && user.item.dataValues.id){
        res.send(methods.successResponse(
            req.body.id?'Update user successfully':'Created user successfully', {
                data: user
            }
        ));
    } else {
        res.send(methods.errorResponse("User not created"));
    }
};

UsersController.changePassword = async(req, res) => {
    const user = await UsersLogic.changePassword(req.body);
    res.send(methods.successResponse(
        'Password changed successfully', {
            data: "success"
        }
    ));
};

UsersController.returnStates = async(req, res) => {
    const states = await UsersLogic.getStates();
    res.send(methods.successResponse(
        'States of Iraq', {
            data: states
        }
    ));
};

UsersController.getUserById = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const user = await UsersLogic.findUserById(id);
    res.send(methods.successResponse(
        'Get User By Id', {
            data: user
        }
    ));
};

UsersController.getAllUsers = async(req, res) => {
    const user = await UsersLogic.getAllUsers();
    res.send(methods.successResponse(
        'Get All Users', {
            data: user
        }
    ));
};

UsersController.sendEmail = async(req, res) => {
    const user = await UsersLogic.sendEmail(req.body);
    res.send(methods.successResponse(
        'Email sent successfully', {
            data: "success"
        }
    ));
};

UsersController.verifyEmailCode = async(req, res) => {
    const emailMessage = await UsersLogic.verifyEmailCode(req.body);
    res.send(methods.successResponse(
        emailMessage, {
            data: "success"
        }
    ));
};

UsersController.PasswordReset = async(req, res) => {
    const user = await UsersLogic.PasswordReset(req.body);
    res.send(methods.successResponse(
        'Password changed successfully', {
            data: "success"
        }
    ));
};

UsersController.editUser = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    let data = req.body;
    data.id = id;
    const userDetail = await UsersLogic.editUser(data);
    let user = {
        id: userDetail.item.dataValues.id,
        first_name: userDetail.item.dataValues.first_name,
        middle_name: userDetail.item.dataValues.middle_name,
        last_name: userDetail.item.dataValues.last_name,
        gender: userDetail.item.dataValues.gender,
        phone: userDetail.item.dataValues.phone,
        email:userDetail.item.dataValues.email,
        date_of_birth: userDetail.item.dataValues.date_of_birth,
        state: userDetail.item.dataValues.state,
        language: userDetail.item.dataValues.language,
        username: userDetail.item.dataValues.username,
    }
    res.send(methods.successResponse(
        req.body.id?'Update user successfully':'Created user successfully', {
            data: {
                item: user 
            }
        }
    ));
};
module.exports = UsersController