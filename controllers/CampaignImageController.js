const methods = require('../helpers/methods')
const Controller = require('./Controller')

const { CampaignImageLogic } = require('../logic');

class CampaignImageController extends Controller {
    //
}

CampaignImageController.createCampaignImage = async(req, res) => {
    let data = req.body;
    let newCampaign = await CampaignImageLogic.createCampaignImages(data, req.user.id);
    res.send(methods.successResponse(
        data.id ? 'Campaign Images Updated' : 'Campaign Images Created', {
            data: newCampaign
        }
    ))
};

module.exports = CampaignImageController