const methods = require('../helpers/methods')
const Controller = require('./Controller')
const { AppLogic } = require('../logic');

class IndexController extends Controller {
    //
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
IndexController.index = async(req, res) => {
    res.render('index', {
        title: 'Home',
        helpers: {
            isPath: value => value === 'home'
        }
    });
}

IndexController.getHomePageData = async(req, res) => {
    const HomePageLogic = await AppLogic.HomePageLogic(req.user);
    res.send(methods.successResponse(
        'Mobile APP Home Page', {
            data: HomePageLogic
        }
    ));
};

IndexController.getAppDetail = async(req, res) => {
    const AppDetail = await AppLogic.getAppDetail(req.user);
    res.send(methods.successResponse(
        'Mobile APP Detail', {
            data: AppDetail
        }
    ));
};

module.exports = IndexController