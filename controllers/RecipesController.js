const methods = require('../helpers/methods')
const Controller = require('./Controller')
const { RecipeLogic } = require('../logic');

class RecipesController extends Controller {
    //
}

RecipesController.getAllRecipes = async(req, res) => {
    const recipes = await RecipeLogic.getAllRecipes();
    res.send(methods.successResponse(
        'Recipes', {
            data: recipes
        }
    ))
}
RecipesController.getRecipeByID = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const recipes = await RecipeLogic.getRecipeByID(id);
    res.send(methods.successResponse(
        'Recipes', {
            data: recipes
        }
    ))
}

RecipesController.createRecipe = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let newRecipe = await RecipeLogic.createRecipe(data);
    res.send(methods.successResponse(
        data.id ? 'Recipe Updated' : 'Recipe Created', {
            data: newRecipe
        }
    ))
};

RecipesController.deleteMultipleRecipes = async(req, res) => {
    let data = req.body;
    let allRecipe = await RecipeLogic.deleteMultipleRecipes(data);
    res.send(methods.successResponse(
        'Recipe Deleted', {
            data: [null]
        }
    ));
};

RecipesController.getRecipeByCategory = async(req, res) => {
    let itemCategory = req.params.category || req.body.category || 0;
    const recipes = await RecipeLogic.getRecipeByCategory(itemCategory);
    res.send(methods.successResponse(
        'Recipes By Category', {
            data: recipes
        }
    ))
}
module.exports = RecipesController