const methods = require('../helpers/methods')
const Controller = require('./Controller')
const { ItemCategoryLogic } = require('../logic');

class ItemCategoryController extends Controller {
    //
}

ItemCategoryController.getProductCategories = async(req, res) => {
    const categories = await ItemCategoryLogic.getProductCategories()
    res.send(methods.successResponse(
        'Product categories', {
            data: categories
        }
    ))
}

module.exports = ItemCategoryController