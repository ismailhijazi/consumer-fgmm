const methods = require('../helpers/methods')
const Controller = require('./Controller')

const { ProductImageLogic } = require('../logic');

class ProductImageController extends Controller {
    //
}

ProductImageController.createProductImages = async(req, res) => {
    let data = req.body;
    let newProductImages = await ProductImageLogic.createProductImages(data, req.user.id);
    res.send(methods.successResponse(
        data.id ? 'Product Images Updated' : 'Product Images Created', {
            data: newProductImages
        }
    ))
};

module.exports = ProductImageController