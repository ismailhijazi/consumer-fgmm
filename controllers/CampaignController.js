const methods = require('../helpers/methods')
const Controller = require('./Controller')

const { CampaignLogic, ImageLogic, CampaignEntryLogic } = require('../logic');

class CampaignController extends Controller {
    //
}

CampaignController.getAllActiveCampaigns = async(req, res) => {
    const activeCampaigns = await CampaignLogic.getActiveCampaigns()
    res.send(methods.successResponse(
        'Active campaigns', {
            data: activeCampaigns
        }
    ))
}

CampaignController.createCampaign = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let newCampaign = await CampaignLogic.createCampaign(data);
    res.send(methods.successResponse(
        data.id ? 'Campaign Updated' : 'Campaign Created', {
            data: newCampaign
        }
    ))
};

CampaignController.getCampaignByID = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const campaign = await CampaignLogic.getCampaignByID(id);
    res.send(methods.successResponse(
        'Campaign by id', {
            data: campaign
        }
    ));
};

CampaignController.competitionEntry = async(req, res) => {
    const uploadResponse = await ImageLogic.uploadImage(req, res, req.user, 3);
    const entry = await CampaignEntryLogic.enterIntoCampaignCompetition(req, req.user, uploadResponse)
    res.send(methods.successResponse(
        'Enrolled into contest', {
            data: entry
        }
    ));
};

CampaignController.deleteCampaign = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    let campaign = await CampaignLogic.deleteCampaign(id);
    res.send(methods.successResponse(
        'Campaign Deleted', {
            data: campaign
        }
    ))
};

CampaignController.deleteMultipleCampaign = async(req, res) => {
    let data1 = req.body;
    let campaign = await CampaignLogic.deleteMultipleCampaign(data1);
    res.send(methods.successResponse(
        'Campaign Deleted', {
            data: campaign
        }
    ))
};

module.exports = CampaignController