const jwt = require('jsonwebtoken');

const methods = require('../helpers/methods')
const Controller = require('./Controller')

const { UsersRecipeLogic } = require('../logic');

class UsersRecipeController extends Controller {
    //
}

UsersRecipeController.createUsersRecipe = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let newRecipe = await UsersRecipeLogic.createUsersRecipe(data);
    res.send(methods.successResponse(
        data.id ? 'Recipe Updated' : 'Recipe Created', {
            data: newRecipe
        }
    ))
};


UsersRecipeController.deleteUsersRecipe = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const Recipes = await UsersRecipeLogic.deleteUsersRecipe(id);
    res.send(methods.successResponse(
        'Delete Favourite Recipes', {
            data: Recipes
        }
    ));
};

UsersRecipeController.getAllUsersRecipe = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    const Recipes = await UsersRecipeLogic.getAllUsersRecipe(data);
    res.send(methods.successResponse(
        'Get All Favourite Recipe', {
            data: Recipes
        }
    ));
};

module.exports = UsersRecipeController