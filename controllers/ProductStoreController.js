const methods = require('../helpers/methods')
const Controller = require('./Controller')

const { ProductStoreLogic } = require('../logic');

class ProductStoreController extends Controller {
    //
}

ProductStoreController.createProductStores = async(req, res) => {
    let data = req.body;
    let newProductStores = await ProductStoreLogic.createProductStores(data, req.user.id);
    res.send(methods.successResponse(
        data.id ? 'Product Stores Updated' : 'Product Stores Created', {
            data: newProductStores
        }
    ))
};

module.exports = ProductStoreController