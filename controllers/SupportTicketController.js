const methods = require('../helpers/methods')
const Controller = require('./Controller')
const { SupportTicketLogic } = require('../logic');

class SupportTicketController extends Controller {
    //
}

SupportTicketController.getAllSupportTicketsForUser = async(req, res) => {
    let data = req.body;
    data['userID'] = req.user.id;
    const allSupportTicketsByUser = await SupportTicketLogic.getSupportTicketsForUser(data)
    res.send(methods.successResponse(
        'Get all support tickets for user', {
            data: allSupportTicketsByUser
        }
    ))
}

SupportTicketController.createSupportTicket = async(req, res) => {
    let data = req.body;
    data['userID'] = req.user.id;
    const newticket = await SupportTicketLogic.createSupportTicket(data);
    res.send(methods.successResponse(
        data.id ? 'Ticket Updated' : 'Ticket Created', {
            data: newticket
        }
    ))
};

SupportTicketController.getTicketByID = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const ticket = await SupportTicketLogic.getTicketByID(id);
    res.send(methods.successResponse(
        'Ticket by id', {
            data: ticket
        }
    ));
};

module.exports = SupportTicketController