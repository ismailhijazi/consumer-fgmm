const methods = require('../helpers/methods')
const Controller = require('./Controller')
const { TypesLogic } = require('../logic');

class TypesController extends Controller {
    //
}

TypesController.getAllTypes = async(req, res) => {
    const currentTypes = await TypesLogic.getAllTypes()
    res.send(methods.successResponse(
        'All Types', {
            data: currentTypes
        }
    ))
}

TypesController.createTypes = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let types = await TypesLogic.createTypes(data)
    res.send(methods.successResponse(
        data.id ? 'Category Updated' : 'Category Created', {
            data: types
        }
    ))
};

TypesController.getTypesByID = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const types = await TypesLogic.getTypesByID(id);
    res.send(methods.successResponse(
        'Types by id', {
            data: types
        }
    ));
};

TypesController.deleteTypes = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const types = await TypesLogic.deleteTypes(id);
    res.send(methods.successResponse(
        'Category Deleted', {
            data: types
        }
    ));
};

TypesController.getAllTypesName = async(req, res) => {
    const currentTypes = await TypesLogic.getAllTypesName()
    res.send(methods.successResponse(
        'All Types', {
            data: currentTypes
        }
    ))
}

TypesController.deleteMultipleTypes = async(req, res) => {
    let data = req.body;
    const types = await TypesLogic.deleteMultipleTypes(data);
    res.send(methods.successResponse(
        'Category Deleted', {
            data: types
        }
    ));
};
module.exports = TypesController