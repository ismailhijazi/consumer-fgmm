const jwt = require('jsonwebtoken');

const methods = require('../helpers/methods')
const Controller = require('./Controller')

const { StatesLogic } = require('../logic');

class StatesController extends Controller {
    //
}

StatesController.getAllStates = async(req, res) => {
    const states = await StatesLogic.getAllStates();
    res.send(methods.successResponse(
        'States of Iraq', {
            data: states
        }
    ));
};

module.exports = StatesController