const methods = require('../helpers/methods')
const Controller = require('./Controller')

const { RecipeReviewsLogic } = require('../logic');

class RecipeReviewsController extends Controller {
    //
}

RecipeReviewsController.createRecipeReviews = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let RecipeReviews = await RecipeReviewsLogic.createRecipeReviews(data, req.user.id);
    res.send(methods.successResponse(
        data.id ? 'Recipe Review Updated' : 'Recipe Review Created', {
            data: RecipeReviews.item
        }
    ))
};

RecipeReviewsController.getAllRecipeReviewsByRecipeId = async(req, res) => {
    let id = req.params.id || req.body.id || 0;
    const recipeReviews = await RecipeReviewsLogic.getAllRecipeReviewsByRecipeId(id);
    res.send(methods.successResponse(
        'Recipe Reviews', {
            data: recipeReviews
        }
    ))
};

RecipeReviewsController.getAllRecipeReviews = async(req, res) => {
    let data = req.body;
    const recipeReviews = await RecipeReviewsLogic.getAllRecipeReviews(data);
    res.send(methods.successResponse(
        'Recipe Reviews', {
            data: recipeReviews
        }
    ))
};


RecipeReviewsController.updateRecipeReviewStatus = async(req, res) => {
    let data = req.body;
    data.userID = req.user.id;
    let RecipeReviews = await RecipeReviewsLogic.updateRecipeReviewStatus(data, req.user.id);
    res.send(methods.successResponse(
        data.id ? 'Recipe Review Updated' : 'Recipe Review Created', {
            data: RecipeReviews.item
        }
    ))
};


module.exports = RecipeReviewsController