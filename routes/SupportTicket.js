const SupportTicketController = require('../controllers/SupportTicketController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/support-ticket', SupportTicketController.getAllSupportTicketsForUser)
    app.post('/api/support-ticket', authMiddleware, SupportTicketController.createSupportTicket)
    app.get('/api/support-ticket/:id', SupportTicketController.getTicketByID)
}