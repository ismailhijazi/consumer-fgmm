const ProductsController = require('../controllers/ProductsController')
const RecipesController = require('../controllers/RecipesController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/recipe/:id',RecipesController.getRecipeByID)
    app.get('/api/recipes', RecipesController.getAllRecipes)
    app.post('/api/recipes', authMiddleware, RecipesController.createRecipe)
    app.post('/api/delete-recipes', authMiddleware, RecipesController.deleteMultipleRecipes)
    app.get('/api/recipes/:category', RecipesController.getRecipeByCategory)

}