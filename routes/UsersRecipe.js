const UsersRecipeController = require('../controllers/UsersRecipeController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/get-favourite-recipe', authMiddleware, UsersRecipeController.getAllUsersRecipe);
    app.post('/api/create-favourite-recipe', authMiddleware, UsersRecipeController.createUsersRecipe);
    app.post('/api/delete-favourite-recipe', authMiddleware, UsersRecipeController.deleteUsersRecipe);

}