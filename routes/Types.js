const TypesController = require('../controllers/TypesController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/categories', TypesController.getAllTypes)
    app.post('/api/categories', authMiddleware, TypesController.createTypes)
    app.get('/api/categories/:id', TypesController.getTypesByID)
    app.delete('/api/categories/:id', authMiddleware, TypesController.deleteTypes)
    app.get('/api/categories-name', TypesController.getAllTypesName)
    app.post('/api/delete-categories', authMiddleware, TypesController.deleteMultipleTypes)


}