const ProductImageController = require('../controllers/ProductImageController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.post('/api/product-images', authMiddleware, ProductImageController.createProductImages)

}