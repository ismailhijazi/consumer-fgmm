const CampaignController = require('../controllers/CampaignController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/campaign', CampaignController.getAllActiveCampaigns)
    app.post('/api/campaign', authMiddleware, CampaignController.createCampaign)
    app.get('/api/campaign/:id', authMiddleware, CampaignController.getCampaignByID)
    app.post('/api/competition-entry', authMiddleware, CampaignController.competitionEntry)
    app.delete('/api/campaign/:id', authMiddleware, CampaignController.deleteCampaign)
    app.post('/api/delete-campaign', authMiddleware, CampaignController.deleteMultipleCampaign)
}