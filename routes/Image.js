const ImageController = require('../controllers/ImageController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/image', ImageController.getAllImagesUploadedByUser)
    app.post('/api/image/upload', authMiddleware, ImageController.uploadImage)
    app.get('/api/image/previous-uploads',  ImageController.getAllImagesUploadedByUser)
    app.post('/api/image/delete', authMiddleware, ImageController.deleteImages)

}