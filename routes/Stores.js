const StoresController = require('../controllers/StoresController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/stores', StoresController.getAllStores)
    app.post('/api/stores', authMiddleware, StoresController.createStores)
    app.get('/api/stores/:id', StoresController.getStoresByID)
    app.delete('/api/stores/:id', authMiddleware, StoresController.deleteStores)
    app.get('/api/stores-name', StoresController.getAllStoresName)
    app.post('/api/delete-stores', authMiddleware, StoresController.deleteMultipleStores)
}