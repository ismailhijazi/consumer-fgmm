const IndexController = require('../controllers/IndexController')
const UsersController = require('../controllers/UsersController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/', IndexController.index);
    app.post('/login', UsersController.login);
    app.post('/password-reset', UsersController.changePassword);
    app.post('/sign-up', UsersController.create);
    app.get('/api/user/:id', UsersController.getUserById);
    app.get('/api/users', UsersController.getAllUsers);
    app.post('/adminlogin', UsersController.adminlogin);
    app.post('/forgot-password-email', UsersController.sendEmail);
    app.post('/verify-email-code', UsersController.verifyEmailCode);
    app.post('/api/user-edit/:id', authMiddleware, UsersController.editUser);
    app.post('/change-password', authMiddleware, UsersController.changePassword);


}