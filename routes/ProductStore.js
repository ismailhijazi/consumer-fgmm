const ProductStoreController = require('../controllers/ProductStoreController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.post('/api/product-stores', authMiddleware, ProductStoreController.createProductStores)

}