const NewsController = require('../controllers/NewsController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/news', NewsController.getAllNews)
    app.get('/api/published-news', NewsController.getAllPublishedNews)
    app.post('/api/news', authMiddleware, NewsController.createNews)
    app.get('/api/news/:id', NewsController.getNewsByID)
    app.get('/api/feature-news', NewsController.getFeatureNews)
    app.delete('/api/news/:id', authMiddleware, NewsController.deleteNews)
    app.post('/api/delete-news', authMiddleware, NewsController.deleteMultipleNews)


}