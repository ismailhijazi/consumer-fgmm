const IndexController = require('../controllers/IndexController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/home-page', IndexController.getHomePageData)
    app.get('/api/app-detail', IndexController.getAppDetail)

}