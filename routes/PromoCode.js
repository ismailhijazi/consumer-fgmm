const PromoCodeController = require('../controllers/PromoCodeController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/promo-code', PromoCodeController.getAllActivePromos)
    app.post('/api/promo-code', authMiddleware, PromoCodeController.createPromoCode)
    app.get('/api/promo-code/previous-entry', authMiddleware, PromoCodeController.getPreviouslyEnteredPromoCodes)
    app.get('/api/promo-code/:id', PromoCodeController.getPromoByID)
    app.post('/api/promo-code/validate', PromoCodeController.validatePromo)
    app.post('/api/promo-code/apply', authMiddleware, PromoCodeController.applyPromo)
    app.post('/api/upload-promo', authMiddleware, PromoCodeController.createPromoCodeByCampaign)

}