const CampaignImageController = require('../controllers/CampaignImageController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.post('/api/campaign-images', authMiddleware, CampaignImageController.createCampaignImage)

}