const ItemCategoryController = require('../controllers/ItemCategoryController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/product-types', ItemCategoryController.getProductCategories)
}