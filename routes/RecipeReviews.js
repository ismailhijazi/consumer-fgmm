const RecipeReviewsController = require('../controllers/RecipeReviewsController')
const RecipesController = require('../controllers/RecipesController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/recipe-reviews/:id',RecipesController.getRecipeByID)
    app.get('/api/recipe-reviews-recipeId/:id', RecipeReviewsController.getAllRecipeReviewsByRecipeId)
    app.get('/api/recipe-reviews', RecipeReviewsController.getAllRecipeReviews)
    app.post('/api/create-recipe-reviews', authMiddleware, RecipeReviewsController.createRecipeReviews)
    app.post('/api/update-recipe-review-status', authMiddleware, RecipeReviewsController.updateRecipeReviewStatus)

}