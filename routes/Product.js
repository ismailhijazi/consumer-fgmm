const ProductsController = require('../controllers/ProductsController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/products/:category', ProductsController.getProductByCategories)
    app.get('/api/product/:id', ProductsController.getProductByID)
    app.get('/api/products', ProductsController.getAllProducts)
    app.post('/api/products', authMiddleware, ProductsController.createProducts)
    app.post('/api/delete-products', authMiddleware, ProductsController.deleteMultipleProducts)
    app.get('/api/featured-products', ProductsController.getAllFeaturedProducts)
    app.get('/api/products-name', ProductsController.getAllProductName)


}