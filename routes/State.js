const StatesController = require('../controllers/StatesController')

module.exports = function(app, authMiddleware) {
    /**
     * static URLS to be on top..
     */
    app.get('/api/states', StatesController.getAllStates);
}